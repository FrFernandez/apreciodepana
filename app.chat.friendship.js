var express = require('express'),
	compression = require('compression'),
	minify = require('express-minify'),
	cors = require('cors'),
	path = require('path'),
	logger = require('morgan'),
	ObjectID = require('mongoskin').ObjectID,
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	User = require('./routes/objects/user'),
	Chat = require('./routes/objects/chat'),
	Message = require('./routes/objects/message'),
	Notification = require('./routes/objects/notification'),
	Server = require('socket.io'),
	io = new Server(),
	app = express(),
	NodeGeocoder = require('node-geocoder'),
	geocoder = NodeGeocoder({
		provider: 'google',
		httpAdapter: 'https',
		apiKey: 'AIzaSyDxPb9rcUgWEK2OBqHtI67qPKSfQb998Hc'
	}),
	corsOptions = {
		origin: ["*"],
		optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
	};
	
User = new User();
Chat = new Chat();
Message = new Message();
Notification = new Notification();


User.update({}, {"$set":{online:false}}, {multi: true});

app.use(compression());	
app.use(cors(corsOptions));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

io.origins('*:*');

var clients = [];

//Chat.update({_id: new ObjectID("590d1cb9fccd9cf61347b150")}, {"$set": {indexH: 0, indexU: 0}}, {multi: true});
//Chat.update({_id: new ObjectID("590d4eb826232123f1746a0f")}, {"$set": {indexH: 1, indexU: 1}}, {multi: true});


function insertMessage(mID, mB, findTo, unread, chat, user){
	return Message.insert(mB, function(success, message){
		if(success){
			var toNotFound = true;
			
			for(let i = 0; i < clients.length; i++){
				if(clients[i].userID.equals(mID))
					for(let j = 0; j < clients[i].sockets.length; j++)
						io.to(clients[i].sockets[j]).emit("byYou", message);
				else if(clients[i].userID.equals(findTo)){
					toNotFound = false;
					message.bep = true;
					for(let j = 0; j < clients[i].sockets.length; j++){
						io.to(clients[i].sockets[j]).emit("toYou", message);
						message.bep = false;
					}
				}
			}
			
			//Chat.update({_id: mB.chat}, unread);
			Chat.update({_id: mB.chat}, {"$set":{lastActive: Date.now()}});
			
			if(toNotFound){
				Notification.insert({
					subject: "Has recibido un nuevo mensaje de "+user.name,
					message: message.message,
					user: findTo,
					fromUser: mID
				});
			}
			return;
		}
	});
}
			

io.on('connection', function(socket, next){
	return User.checkHash(socket.handshake.query.token, socket.handshake.query.tokensecret,function(user){
		if(user){
            var notFound = true,
				mID = new ObjectID(user._id);
				
				
			for(let i = 0; i < clients.length; i++)
				if(clients[i].userID.equals(mID)){
					clients[i].sockets.push(socket.id);
					notFound = false;
				}
				
			if(notFound){
				clients.push({
					userID: mID,
					sockets: [socket.id]
				});
				User.update({_id: mID}, {"$set":{online: true}});
			}
			
			socket.on('disconnect', function(){
				for(var i = 0; i < clients.length; i++){
					if(clients[i].userID.equals(mID)){
						for(var j = 0; j < clients[i].sockets.length ; j++){
							if(clients[i].sockets[j] == socket.id){
								clients[i].sockets.splice(j,1);
								if(clients[i].sockets.length === 0){
									User.update({_id: clients[i].userID}, {"$set":{online: false}});
									clients.splice(i,1);
								}
							}
							break;
						}
					}
					break;
				}
			});
			
			
			socket.on('stoppedTyping', function(data){
				data.to = new ObjectID(data.to);
				
				let find = {_id: data.to, users: mID};
				
				return Chat.findOne(find, function(success, chat){
					if(success){
						var findMe;
						if(mID.equals(new ObjectID(chat.users[0]))){
							findMe = new ObjectID(chat.users[0]);
						}else{
							findMe = new ObjectID(chat.users[1]);
						}
						
						for(let i = 0; i < clients.length; i++){
							if(clients[i].userID.equals(findMe)){
								for(let j = 0; j < clients[i].sockets.length; j++){
									io.to(clients[i].sockets[j]).emit("stoppedTyping", {chat: data.to});
								}
							}
						}
					}
				});
			});
			
			
			socket.on('typing', function(data){
				data.to = new ObjectID(data.to);
				let find = {_id: data.to, users: mID};
				
				return Chat.findOne(find, function(success, chat){
					if(success){
						var findMe;
						if(mID.equals(new ObjectID(chat.users[0]))){
							findMe = new ObjectID(chat.users[0]);
						}else{
							findMe = new ObjectID(chat.users[1]);
						}
						
						for(let i = 0; i < clients.length; i++){
							if(clients[i].userID.equals(findMe)){
								for(let j = 0; j < clients[i].sockets.length; j++){
									io.to(clients[i].sockets[j]).emit("typing", {chat: data.to});
								}
							}
						}
					}
				});
			});
			
			socket.on('startChat', function(data){
			
				data.to = new ObjectID(data.to);
				let cB = {};
				if(isHouse){
					cB.house = mID;
					cB.user = data.to;
				}else{
					cB.house = data.to;
					cB.user = mID;
				}
			
				return Chat.insert(cB, function(success, id){
					if(success){
						return Chat.getFull({_id: new ObjectID(id)}, function(success, chat){
							for(let i = 0; i < clients.length; i++){
								if(clients[i].userID.equals(mID) || clients[i].userID.equals(data.to)){
									console.log(clients[i]);
									for(let j = 0; j < clients[i].sockets.length; j++){
										io.to(clients[i].sockets[j]).emit("newChat", chat);
									}
								}
							}
							return;
						});
					}
				});
			});
			
			socket.on('mmessage', function(data){
				data.to = new ObjectID(data.to);
				
				let find = {_id: data.to, users: mID};
				return Chat.findOne(find, function(success, chat){
					if(success){
						
						var findTo, unread = {"$inc":{}};
						if(mID.equals(new ObjectID(chat.users[0]))){
							//unread["$inc"] = {unreadU: 1};
							findTo = new ObjectID(chat.users[0]);
						}else{
							//unread["$inc"] = {unreadH: 1};
							findTo = new ObjectID(chat.users[1]);
						}
					
						var mB = {
							chat: data.to,
							message: data.message,
							user: mID,
							type: 1
						};
						return insertMessage(mID, mB, findTo, unread, chat, user);
					}
				});
			});
			//return next();
		}else{
			//console.log(socket);
			socket.disconnect();
		}
	});
});

module.exports = {
	app: app,
	io: io
};