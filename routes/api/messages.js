var express = require('express'),
	router = express.Router(),
	ObjectID = require('mongoskin').ObjectID,
	Chat = require('../objects/chat'),
	Message = require('../objects/message'),
	User = require('../objects/user');

	User = new User();
	Chat = new Chat();
	Message = new Message();
	
	
router.route('/chat/:id')
	.get(function(req,res){
        var body = req.query,
			skip = body.start || 0,
			limit = body.limit || 20,
            id = req.params.id;
			
		limit = parseInt(limit);
		skip = parseInt(skip);
		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			
			if(req.user){
				var find = {_id:id, users: new ObjectID(req.user._id)};
				/*
				find.$or = [{user: new ObjectID(user._id)},{house: new ObjectID(user._id)}];
				*/
				return Chat.findOne(find, function(success, notis){
					if(success){
						find = {chat: id};
						
						if(body.minid && ObjectID.isValid(body.minid)){
							find._id = {"$lt": new ObjectID(body.minid)};
						}
						
						return Message.getMultiFull(find, {time:-1}, skip, limit, function(success, notis){
							if(success){
								Message.count(find, function(success, count){
									if(success){
										res.json({e:0,items:notis,total:count});
									}else
										res.json({e:2, mes:count});
								});
							}else
								res.json({e:1, mes: notis});
						});
					}else
						res.json({e:1, mes: notis});
				});
			}else{
				res.json({e:1,mes:"Token invalido"});
			}
		}else
			return res.json({e:1, mes: "ID invalido"});
	});
module.exports = router;