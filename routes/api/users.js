var express = require('express'),
	router = express.Router(),
	fileUtils = require('../utils/fileUtils'),
	mailer = require('../utils/mailer'),
	multer = require('multer'),
	request = require('request'),
	upload = multer({ dest: './temp/' }),
	ObjectID = require('mongoskin').ObjectID,
	sanetize = require('../middlewares/sanetize'),
	Friendship = require('../objects/friendship'),
	Follow = require('../objects/follow'),
	User = require('../objects/user');

	User = new User();
	Follow = new Follow();
	Friendship = new Friendship();

router.use(sanetize());

router.route('/forgotpassword')
	.post(function(req,res){
		var mail = new RegExp(["^", req.body.mail, "$"].join(""), "i");
		User.findOne({"mail":mail,"valid":true},function(success,user){
			if(success){
				var bitacora = User.getBitacora();
				User.update({_id:new ObjectID(user._id)},{$push:{bitacora:bitacora}},{},function(success, mes){
					if (success) {
						var mailOptions = mailer.forgotMail(user.name, bitacora.token, bitacora.tokenSecret);
                            mailOptions.to = req.body.mail;
                            mailer.sendMail(mailOptions);
						res.json({e:0});
					}else {
						res.json({e:3,mes:mes});
					}
				});
			}else
				return res.json({e:1, mes: user});
		});
	});

router.route('/validate/:mail/:validationtoken')
	.put(function(req, res){
		var mail = req.params.mail,
			token = req.params.validationtoken;

		User.findOne({mail:mail, validateToken: token}, function(success, user){
			if(success){
				return User.update({_id: new ObjectID(user._id)}, {"$set":{valid: true}}, {}, function(success){
					if(success)
						return res.json({e:0});
					else
						return res.json({e:1, mes: "Fue imposible validar tu cuenta en este momento"});
				});
			}else
				return res.json({e:1, mes: "Código no encontrado"});
		});
	});

router.route('/binddevice')
    .post(function(req,res){
        var token = req.body.token,
            tokenSecret = req.body.tokensecret,
            uuid = req.body.uuid,
            googleid = req.body.googleid;

		if(req.user){
			var data = {
				uuid: uuid,
				googleToken: googleid
			};

			User.update({"devices.uuid":uuid},{"$pull":{"devices":{uuid:uuid}}}, {}, function(success){
				User.update({_id:new ObjectID(user._id)},{"$push":{devices:data}}, {}, function(success, mes){
					if(success){
						res.json({e:0});
					}else
						res.json({e:2,mes:"Error al actualizar ID"});
				});
			});
		}else{
			res.json({e:2,mes:"Operación no permitida"});
		}
    });

router.route('/searchp')
	.get(function(req,res){
		var body = req.query,
			token = body.token,
			tokenSecret = body.tokensecret,
			start = body.start || 0,
			limit = body.limit || 20,
			term = body.term && body.term.length > 0 ? body.term : false ;

		start = parseInt(start);
		limit = parseInt(limit);

		return User.count(body, function(success, count){
			if(success)
				if(count>0){
					return User.getMultiFull(body, {plan:-1,"rating.total":-1, searchName: 1}, start, limit, function(success, users){
						if(success){
							return res.json({e:0, items:users, total: count});
						}else
							return res.json({e:1, mes:users});
					});
				}else
					return res.json({e:0, items: [], total: 0});
			else
				return res.json({e:1, mes:count});
		});
		return;
	});

router.route('/search')
	.get(function(req,res){
		var body = req.query,
			token = body.token,
			tokenSecret = body.tokensecret,
			start = body.start || 0,
			limit = body.limit || 20,
			term = body.term && body.term.length > 0 ? body.term : false ;

		start = parseInt(start);
		limit = parseInt(limit);

		/*
		if(req.user && User.isAdmin(req.user)){*/

			var find = {};
			if(term)
				find["$or"] = [{mail:{"$regex":new RegExp(term,'i')}},{name:{"$regex":new RegExp(term,'i')}}];

			if(body.priv)
				find.priv = parseInt(body.priv);


			return User.count(find, function(success, count){
				if(success)
					if(count>0){
						return User.getMultiFull(find,{ name:1, mail: 1}, start, limit, function(success, users){
							if(success){
								return res.json({e:0, items:users, total: count});
							}else
								return res.json({e:1, mes:users});
						});
					}else
						return res.json({e:0, items: [], total: 0});
				else
					return res.json({e:1, mes:count});
			});

		/*}else
			return res.json({e:1,mes:"Token invalida"});*/
	});

router.route('/id/:id')
	.get(function(req,res){
		var token = req.query.token,
			tokenSecret = req.query.tokensecret,
			id = req.params.id;

		if(ObjectID.isValid(id)){
			id = new ObjectID(id);
			return User.getFull(id, function(success, user){
				if(success){
					if(req.user){
						return Friendship.findOne(
							{users:
								{"$size":2,
								"$all": [new ObjectID(req.user._id), new ObjectID(id)]
								}
							}, function(success, friendship){
								user.friends = success;

							return Follow.findOne({follower: new ObjectID(req.user._id), followed: id}, function(success, follows){
								user.youfollow = success;

								return Follow.findOne({followed: new ObjectID(req.user._id), follower: id}, function(success, follows){
									user.followsyou = success;
									return res.json({e:0, item: user});
								});
							});
						});
					}else
						return res.json({e:0, item: user});
				}else
					return res.json({e:2, mes: user});
			});
		}else
			return res.json({e:2, mes: "ID Invalido"});
	})
	.put(upload.fields([{name: 'cover', maxCount:1},
		{name: 'photo', maxCount:1}]),function(req,res,next){
		if(!req.files || (!req.files['photo'] || !req.files['photo'][0]) && (!req.files['cover'] || !req.files['cover'][0]))
			return next();

		var body = req.body,
			token = body.token,
			tokenSecret = body.tokensecret,
			id = req.params.id;

		if(req.user && User.isAdmin(req.user)){
			if(ObjectID.isValid(id)){
				User.findOne({_id: new ObjectID(id)}, function(success, userToUpt){
					if(success){
							var filests = [];
							if(req.files['photo'] && req.files['photo'][0])
								filests.push(req.files['photo'][0]);

							if(req.files['cover'] && req.files['cover'][0])
								filests.push(req.files['cover'][0]);


							fileUtils.saveFile(filests,"image",function(files){
								if(files){
									if(req.files['photo'] && req.files['photo'][0])
										body.photo = files.shift();

									if(req.files['cover'] && req.files['cover'][0])
										body.cover = files.shift();

									return User.doUpdate(req.user, body, userToUpt, function(success, mes){
										if(success)
											return res.json({e:0});
										else
											return res.json({e:1,mes:mes});
									});
								}else
									return res.json({e:3, mes:"Error al guardar archivo"});
							});
					}else
						return res.json({e:2, mes: userToUpt});
				});
			}else
				return res.json({e:1, mes: "ID Invalido"});
		}else
			return res.json({e:1,mes:"Token invalido"});
	})
	.put(function(req,res){
		console.log(req.user)
		var body = req.body,
			token = body.token,
			tokenSecret = body.tokensecret;

		if(req.user && User.isAdmin(req.user)){
			if(ObjectID.isValid(req.params.id)){
				id = new ObjectID(req.params.id);


				let files = [];
				if(body.photo64)
					files.push(body.photo64);
				if(body.cover64)
					files.push(body.cover64);

				if(files.length > 0)
					return fileUtils.saveBase64Files(files, function(files){

						if(files){


							if(body.photo64)
								body.photo = files.shift();
							if(body.cover64)
								body.cover = files.shift();

							return User.doUpdate(req.user, body, userToUpt, function(success, mes){
								if(success)
									if(mes && mes.token)
										return res.json({e:0, token: mes.token, tokensecret: mes.tokenSecret});
									else
										return res.json({e:0});
								else
									return res.json({e:1,mes:mes});
							});
						}else
							return res.json({e:3, mes:"Error al guardar archivo"});
					});
				else
					return User.findOne({_id: new ObjectID(req.params.id)}, function(success, userToUpt){
						if(success){
							return User.doUpdate(req.user, body, userToUpt, function(success, mes){
								if(success)
									return res.json({e:0});
								else
									return res.json({e:1,mes:mes});
							});
						}else
							return res.json({e:2, mes: userToUpt});
					});
			}else
				return res.json({e:1, mes: "ID Invalido"});
		}else
			return res.json({e:1,mes:"Token invalido"});
	})
	.delete(function(req,res){
		var token = req.query.token,
			tokenSecret = req.query.tokensecret,
			id = req.params.id;

		if(ObjectID.isValid(id)){
			id = new ObjectID(id);

			User.checkHash(token, tokenSecret, function(user){
				if(req.user && User.isAdmin(req.user))
					return User.update({_id:id}, {"$set": {status: 2}}, {}, function(success, mes){
						if(success){
							return res.json({e:0});
						}else
							return res.json({e:2, mes: mes});
					});
				else
					return res.json({e:1,mes:"Token invalido"});
			});

		}else
			res.json({e:1, mes: "ID invalido"});
	});

var proccessInsert = function(body, res){
	body.priv = 3;
	switch(body.datasource){
		case 'regular':
			/*if(!body.name ||
				!body.phone ||
				!body.mail ||
				!body.password ||
				body.password.length < 8 ||
				!body.province ||
				!body.locality ||
				!body.priv ||
				!body.photo ||
				isNaN(body.priv) ||
				parseInt(body.priv) < 1 ||
				parseInt(body.priv) > 3 ||
				!ObjectID.isValid(body.province) ||
				!ObjectID.isValid(body.locality)){
					return res.json({
						e:7,
						mes:"Debes enviar todos los campos"
					});
				}*/


			return User.insert(body, function(success, mes){
				if(success)
					res.json({e:0, id:mes});
				else
					return res.json({e:4, mes: mes});
			});
			break;
		case 'fb':
			if(body.access_token){
				var fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name', 'gender', 'picture.width(640)'],
					accessTokenUrl = 'https://graph.facebook.com/v2.5/oauth/access_token',
					graphApiUrl = 'https://graph.facebook.com/v2.5/me?fields=' + fields.join(',');

				return request.get({ url: graphApiUrl, qs: {access_token: body.access_token}, json: true }, function(err, response, profile) {

					if (response.statusCode !== 200) {
						console.log(response);
						return res.json({e:0, mes: "Ha habido un error con Facebook"});
					}

					profile.picture = profile.picture.data.url;
					profile.access_token = body.access_token;
					return User.findOne({ "facebook.id": profile.id }, function(success, user) {
						if(success){
							return res.json({e:0, mes: "Ya existe un pana con este Facebook"});
						}else{
							body.facebook = profile;
							if(body.photo)
								return User.insert(body, function(success, mes){
										if(success)
											res.json({e:0, id:mes});
										else
											return res.json({e:4, mes: mes});
									});
							else
								return fileUtils.downloadAndSave([profile.picture],"image",function(doneFiles){
									if(doneFiles){
										body.photo = doneFiles[0];
										return User.insert(body, function(success, mes){
											if(success)
												res.json({e:0, id:mes});
											else
												return res.json({e:4, mes: mes});
										});
									}else
										return res.json({e:3, mes:"Error al guardar foto"});
								});
						}
					});
				});

			}else
				return res.json({e:3, mes:"Fuente invalida"});
			break;
		case 'google':
			if(body.access_token){
				request.get({ url: 'https://www.googleapis.com/plus/v1/people/me/openIdConnect', headers: { Authorization: 'Bearer ' + body.access_token }, json: true }, function(err, response, profile) {
					if (profile.error) {
						return res.status(500).send({message: profile.error.message});
					}

					profile.access_token = body.access_token;
					profile.picture = profile.picture.replace('sz=50', 'sz=640');
					User.findOne({ "google.sub": profile.sub }, function(success, user) {
						if(success){
							return res.json({e:0, mes: "Ya existe un pana con esta cuenta de Google"});
						}else{
							body.google = profile;
							if(body.photo)
								return User.insert(body, function(success, mes){
									if(success)
										res.json({e:0, id:mes});
									else
										return res.json({e:4, mes: mes});
								});
							else
								return fileUtils.downloadAndSave([profile.picture],"image",function(doneFiles){
									if(doneFiles){
										body.photo = doneFiles[0];
										return User.insert(body, function(success, mes){
											if(success)
												res.json({e:0, id:mes});
											else
												return res.json({e:4, mes: mes});
										});
									}else
										return res.json({e:3, mes:"Error al guardar foto"});
								});
						}
					});
				});
			}else
				return res.json({e:3, mes:"Fuente invalida"});
			break;
		default:
			return res.json({e:3, mes:"Fuente invalida"});
			break;
	}

};

router.route('/')
	.get(function(req,res){
		var token = req.query.token,
			tokenSecret = req.query.tokensecret;

		User.checkHash(token, tokenSecret, function(user){
			if(req.user)
				User.getFull(user._id, function(success, mes){
					if(success){
						delete mes.priv;

						return res.json({e:0, item: mes});
					}else
						return res.json({e:2, mes: mes});
				});
			else
				return res.json({e:1,mes:"Token invalido"});
		});
	})
	.put(upload.fields([{name: 'cover', maxCount:1},
		{name: 'photo', maxCount:1}]),function(req,res,next){

		if(!req.files ||
			(
				(!req.files['photo'] || !req.files['photo'][0]) &&
				(!req.files['cover'] || !req.files['cover'][0])
			)
		)
			return next();

		var body = req.body,
			token = body.token,
			tokenSecret = body.tokensecret;

		if(req.user){
			var filests = [];

			if(req.files['photo'] && req.files['photo'][0])
				filests.push(req.files['photo'][0]);

			if(req.files['cover'] && req.files['cover'][0])
				filests.push(req.files['cover'][0]);


			fileUtils.saveFile(filests,"image",function(files){
				if(files){
					if(req.files['photo'] && req.files['photo'][0])
						body.photo = files.shift();

					if(req.files['cover'] && req.files['cover'][0])
						body.cover = files.shift();

					return User.doUpdate(req.user, body, req.user, function(success, mes){
						if(success){
							if(mes && mes.token)
								return res.json({e:0, token: mes.token, tokensecret: mes.tokenSecret});
							else
								return res.json({e:0});
						}else
							return res.json({e:1,mes:mes});
					});
				}else
					return res.json({e:3, mes:"Error al guardar archivo"});
			});
		}else
			return res.json({e:1,mes:"Token invalido"});
	})
	.put(function(req,res){
		var body = req.body,
			token = body.token,
			tokenSecret = body.tokensecret;


		if(req.user){
			let files = [];
			if(body.photo64)
				files.push(body.photo64);
			if(body.cover64)
				files.push(body.cover64);


			if(files.length > 0)
				return fileUtils.saveBase64Files(files, function(files){

					if(files){
						if(body.photo64)
							body.photo = files.shift();
						if(body.cover64)
							body.cover = files.shift();

						return User.doUpdate(req.user, body, req.user, function(success, mes){
							console.log(arguments);
							if(success)
								if(mes && mes.token)
									return res.json({e:0, token: mes.token, tokensecret: mes.tokenSecret});
								else
									return res.json({e:0});
							else
								return res.json({e:1,mes:mes});
						});
					}else
						return res.json({e:3, mes:"Error al guardar archivo"});
				});
			else
				return User.doUpdate(req.user, body, req.user, function(success, mes){
					if(success)

						if(mes && mes.token)
							return res.json({e:0, token: mes.token, tokensecret: mes.tokenSecret});
						else
							return res.json({e:0});
					else
						return res.json({e:1,mes:mes});
				});
		}else
			return res.json({e:1,mes:"Token invalido"});
	})
	.post(upload.fields([{name: 'cover', maxCount:1},
		{name: 'photo', maxCount:1}]),function(req,res,next){
		var body = req.body;

		if(!req.files ||
			(
				(!req.files['photo'] || !req.files['photo'][0]) &&
				(!req.files['cover'] || !req.files['cover'][0])
			)
		)
		return next();

		var filests = [];

		if(req.files['photo'] && req.files['photo'][0])
			filests.push(req.files['photo'][0]);

		if(req.files['cover'] && req.files['cover'][0])
			filests.push(req.files['cover'][0]);

		return fileUtils.saveFile(filests,"image",function(files){
			if(files){
				if(req.files['photo'] && req.files['photo'][0])
					body.photo = files.shift();

				if(req.files['cover'] && req.files['cover'][0])
					body.cover = files.shift();

				return proccessInsert(body, res);
			}else
				return res.json({e:3, mes:"Error al guardar archivo"});
		});
	})
	.post(function(req,res){

		let files = [];
		if(body.photo64)
			files.push(body.photo64);
		if(body.cover64)
			files.push(body.cover64);

		if(files.length > 0)
			return fileUtils.saveBase64Files(files, function(files){
				if(files){
					if(body.photo64)
						body.photo = files.shift();
					if(body.cover64)
						body.cover = files.shift();

					return proccessInsert(body, res);
				}else
					return res.json({e:3, mes:"Error al guardar archivo"});
			});
		else
			return proccessInsert(req.body, res);
	});

module.exports = router;
