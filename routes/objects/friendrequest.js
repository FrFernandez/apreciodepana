var db = require('../utils/db'),
	collection = db.collection("friendrequest"),
	ObjectID = require('mongoskin').ObjectID,
	User = require(__dirname +'/user'),
	Friendship = require(__dirname +'/friendship'),
	Notification = require(__dirname +'/notification'),
	method = Friendrequest.prototype;

	User = new User();
	Friendship = new Friendship();
	Notification = new Notification();
	
	
function Friendrequest(){

}

method.update = function(query, order, extra, cb){
	return collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else
			return cb ? cb(true, users) : false;
	});
};

method.findOne = function(filter, cb){
	collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else
			return cb ? cb(false, "Friendrequest no encontrada") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, users){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(users){
			var il = users.length, round = 1, i = 0, total = 0, newUsers = [];
			var handleLoop = function(i){
				this.getFull(users[i], function(success, user){
					if(success)
						newUsers[i] = user;
					else
						newUsers[i] = users[i];
					
					if(round == il)
						return cb ? cb(true, newUsers) : false;
					return round++;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, users) : false;
		}
		else return cb ? cb(false, "Friendrequests no encontrados") : false;
	}.bind(this));
};

method.proccess = function(friendrequest, cb){
	return User.getFull(new ObjectID(friendrequest.user), function(success, user){
		if(success)
			friendrequest.user = user;
		User.getFull(new ObjectID(friendrequest.house), function(success, house){
			if(success)
				friendrequest.house = house;
			return cb ? cb(true, user) : false;
		});
	});
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		this.findOne({_id:id}, function(success, friendrequest){
			if(success){
				return this.proccess(friendrequest, cb);
			}else
				return cb ? cb(false, "Friendrequest no encontrado") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.insert = function(data, cb){
	if(ObjectID.isValid(data.from) &&
		ObjectID.isValid(data.to)){
		
		data.from = new ObjectID(data.from);
		data.to = new ObjectID(data.to);
		data.requested = Date.now();
		
		return User.findOne({_id: from}, function(success, user){
			if(success){
				return User.findOne({_id: requested}, function(success, requested){
					if(success){
						return Friendship.findOne({users:{"$size":2, "$all": [data.form, data.to]}}, function(success, friendship){
							if(success){
								return cb ? cb(false, "Ya existe esta amistad") : false;
							}else{
								this.findOne({to: data.from, from: data.to}, function(success, friendreqest){
									if(success){
										return cb ? cb(false, "Este usuario ya te ha solicitado una amistad") : false;
									}else
										return collection.insert(data, function(err, result){
											if(err){
												return cb ? cb(false, "Error al insertar") : false;
											}else{
												Notification.insert({
													subject: "Nuevo solicitud de amistad",
													message: user.username+" quiere ser tu pana",
													type: "friendrequest",
													user: data.to
												});
												return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
											}
										}.bind(this));
								});
							}
						}.bind(this));
					}else
						return cb ? cb(false, "Usuario no encontrada") : false;
				}.bind(this));
			}else
				return cb ? cb(false, "Usuario no encontrado") : false;
		}.bind(this));
	}else
		return cb ? cb(false, "Debes enviar todos los campos") : false;
};

module.exports = Friendrequest;