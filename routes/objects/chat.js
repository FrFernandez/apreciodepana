var db = require('../utils/db'),
	collection = db.collection("chat"),
	ObjectID = require('mongoskin').ObjectID,
	User = require(__dirname +'/user'),
	method = Chat.prototype;

	User = new User();
	
	
function Chat(){

}

method.update = function(query, order, extra, cb){
	return collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else
			return cb ? cb(true, users) : false;
	});
};

method.findOne = function(filter, cb){
	collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else
			return cb ? cb(false, "Chat no encontrada") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);
		
	return promise.toArray(function(err, users){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(users){
			var il = users.length, round = 1, i = 0, total = 0, newUsers = [];
			var handleLoop = function(i){
				this.getFull(users[i], function(success, user){
					if(success)
						newUsers[i] = user;
					else
						newUsers[i] = users[i];
					
					if(round == il)
						return cb ? cb(true, newUsers) : false;
					return round++;
				}.bind(this));
				return;
			}.bind(this);
			
			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, users) : false;
		}
		else return cb ? cb(false, "Chats no encontrados") : false;
	}.bind(this));
};

method.proccess = function(chat, cb){
	return User.getFull(new ObjectID(chat.users[0]), function(success, user){
		if(success)
			chat.users[0] = user;
		User.getFull(new ObjectID(chat.users[1]), function(success, house){
			if(success)
				chat.users[1] = house;
			return cb ? cb(true, user) : false;
		});
	});
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		this.findOne({_id:id}, function(success, chat){
			if(success){
				return this.proccess(chat, cb);
			}else
				return cb ? cb(false, "Chat no encontrado") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.insert = function(data, cb){
	if(ObjectID.isValid(data.users[0]) &&
		ObjectID.isValid(data.users[1])){
		
		data.users = [new ObjectID(data.users[0]), new ObjectID(data.users[1])];
		data.lastActivity = Date.now();
		
		return User.findOne({_id: data.users[0]}, function(success, user){
			if(success){
				return User.findOne({_id: data.users[1]}, function(success, house){
					if(success){
						return this.findOne({users:{"$size":2, "$all": data.users}}, function(success, chat){
							if(success){
								this.update({_id: new ObjectID(chat._id)}, {"$set": {lastActivity: Date.now()}});
								return cb ? cb(true, new ObjectID(chat._id)) : false;
							}else{
								return collection.insert(data, function(err, result){
									if(err){
										return cb ? cb(false, "Error al insertar") : false;
									}else{
										return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
									}
								}.bind(this));
							}
						}.bind(this));
					}else
						return cb ? cb(false, "Usuario no encontrada") : false;
				}.bind(this));
			}else
				return cb ? cb(false, "Usuario no encontrado") : false;
		}.bind(this));
	}else
		return cb ? cb(false, "Debes enviar todos los campos") : false;
};

module.exports = Chat;