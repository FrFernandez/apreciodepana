var db = require('../utils/db'),
	collection = db.collection("transactions"),
	ObjectID = require('mongoskin').ObjectID,
	method = Transaction.prototype,
	Hashids = require("hashids"),
	hashids = new Hashids("apreciodepana");

function Transaction(){

}

method.dateToTime = function(date){
	date=date.split("-");
	return new Date(date[2]+"-"+date[1]+"-"+date[0]).getTime();
}

method.update = function(query, order, extra, cb){
	return collection.update(query, order, extra, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.doUpdate = function(user, id, body, oldstatuss, oldstatusb, cb){
	var data = {};

	if(!ObjectID.isValid(id))
		return cb ? cb(false, "ID Invalido") : false;
	id = new ObjectID(id);

	if(body.statuss && oldstatuss == 1)
		data.statuss = parseInt(body.statuss);

	if(body.statusb && oldstatusb == 1)
		data.statusb = parseInt(body.statusb);

	if(Object.keys(data).length > 0){
		return this.update({_id:id}, {"$set":data}, {}, function(success, mes){
			if(success){
				return cb ? cb(true, data.bitacora[0]) : false;
			}else{
				return cb ? cb(false, mes) : false;
			}
		});
	}else
		return cb ? cb(false, "Debes enviar al menos un campo para actualizar") : false;
};

method.remove = function(filter, cb){
	return collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
};

method.count = function(filter, cb){
	filter = this.preparefind(filter);
	return collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al conseguir total de busqueda") : false;
		}else
			return cb ? cb(true, count) : false;
		return;
	});
};

method.find = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}

	filter = this.preparefind(filter);
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);

	return promise.toArray(function(err, transactions){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(transactions) return cb ? cb(true, transactions) : false;
		else return cb ? cb(false, "Transacciones no encontradas") : false;
	});
};

method.preparefind = function(body){
	var find = {};

	if(body.buyer && ObjectID.isValid(body.buyer))
		find.buyer = new ObjectID(body.buyer)

	if(body.seller && ObjectID.isValid(body.seller))
		find.seller = new ObjectID(body.seller)

	if(body.publication && ObjectID.isValid(body.publication))
		find.publication = new ObjectID(body.publication)

	if(body.mintime){
		if(!find.time) find.time = {}
		find.time["$gte"] = this.dateToTime(body.mintime);
	}


	if(body.statusb)
		find.statusb = parseInt(body.statusb);

	if(body.statuss)
		find.statuss = parseInt(body.statuss);
	if(body.maxtime){
		if(!find.time) find.time = {}
		find.time["$lte"] = this.dateToTime(body.maxtime);
	}
	return find;
};

method.findOne = function(filter, cb){
	return collection.findOne(filter, function(err, user){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al encontrar usuario") : false;
		}else if(user)
			return cb ? cb(true, user) : false;
		else return cb ? cb(false, "Transaction not found") : false;
	});
};

method.getMultiFull = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}
	filter = this.preparefind(filter);
	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);

	return promise.toArray(function(err, items){
		if(err){
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else if(items){
			var il = items.length, round = 1, i = 0, total = 0, newitems = [];
			var handleLoop = function(i){
				this.getFull(items[i], function(success, user){
					if(success)
						newitems[i] = user;
					else
						newitems[i] = items[i];

					if(round == il)
						return cb ? cb(true, newitems) : false;
					round++;
					return;
				}.bind(this));
				return;
			}.bind(this);

			if(il>0)
				for(;i<il;i++)
					handleLoop(i);
			else
				return cb? cb(true, items) : false;
		}
		else return cb ? cb(false, "Usuarios no encontrados") : false;
	}.bind(this));
};

method.proccess = function(item, cb){
	return User.getFull(new ObjectID(item.buyer), function(success, user){
		if(success)
			item.buyer = user;

		return User.getFull(new ObjectID(item.seller), function(success, user){
			if(success)
				item.seller = user;

			return Publication.getFull(new ObjectID(item.publication), function(success, publication){
				if(success)
					item.publication = publication;

				return Report.getFullTransaction(new ObjectID(item._id), function(success, reports){
					if(success)
						item.report = reports;

					if(item.type !== 'undefined' && item.type === 1)
						return PanaPago.getFullTransaction(new ObjectID(item._id), function(success, panaPago){
							if(success)
								item.payments = panaPago;

							return cb ? cb(true, item) : false;
						})
					else return cb ? cb(true, item) : false;
				});
			});
		});
	});
};

method.getFull = function(id, cb){
	if(ObjectID.isValid(id)){
		id = new ObjectID(id);
		return this.findOne({_id:id}, function(success, item){
			if(success){
				return this.proccess(item, cb);
			}else
				return cb ? cb(false, "Cuenta Bancaria no encontrada") : false;
		}.bind(this));
	}else if(id !== null && typeof id === 'object')
		return this.proccess(id, cb);
	else
		return cb ? cb(false, "ID Invalido") : false;
};

method.insert = function(body, cb){

	if(!body.publication || !ObjectID.isValid(body.publication))
		return cb ? cb(false, "Debes enviar a que publicación pertenece esta transacción") : false;

	if(!body.buyer || !ObjectID.isValid(body.buyer))
		return cb ? cb(false, "Debes enviar el comprador esta transacción") : false;

	if(!body.quantity || isNaN(body.quantity))
		return cb ? cb(false, "Debes enviar la cantidad de articulos a comprar") : false;

	var data = {
		publication: new ObjectID(body.publication),
		buyer: new ObjectID(body.buyer),
		time: Date.now(),
		quantity: parseInt(body.quantity),
		type: parseInt(body.type),
		statusb: 1,
		statuss: 1,
		billing: {},
		delivery: {},
		payments: [],
		rate: {
			seller: {
				rate: 0,
				comment: '',
			},
			buyer: {
				rate: 0,
				comment: '',
			}
		},
		miniid: hashids.encode(Date.now()),
	};


	return User.findOne({_id: data.buyer}, function(success, user){
		if(success){
			Publication.findOne({_id: data.publication}, function(success, publication){
				if(success){
					if(data.quantity > publication.stock)
						return cb ? cb(false, "Stock insuficiente") : false;


					data.seller = new ObjectID(publication.user);
					data.price = publication.price;
					data.oldpublication = publication;

					if(data.buyer.equals(data.seller))
						return cb ? cb(false, "No puedes comprar tu propia publicación") : false;

					return collection.insert(data, function(err, result){
						if(err){
							console.log(err);
							return cb ? cb(false, "Error al registrar transacción") : false;
						}else{

							Notification.insert({
								subject: "Felicidades por tu venta",
								message: user.username+" ha comprado "+data.quantity+" "+publication.name,
								type: "transaction",
								meta: new ObjectID(result.insertedIds[0]),
								user: new ObjectID(publication.user)
							});

							if(publication.stock - data.quantity == 0)
								Notification.insert({
									subject: "Alerta de Stock",
									message: "Se ha agotado el stock de tu publicación "+publication.name,
									type: "stock",
									meta: data.publication,
									user: new ObjectID(publication.user)
								});

							Publication.update({_id: data.publication, status:2}, {"$inc": {stock : -data.quantity, sales: 1}});
							return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
						}
					});

				}else
					return cb ? cb(false, publication) : false;
			});
		}else
			return cb ? cb(false, user) : false;
	});
};

method.addRate = function(id, body, cb) {
	this.findOne({_id: id}, (success, item) => {
		let user,
				data;
		if(success){
			if(item.seller.equals(body.user)){
				user = item.buyer;
				item.rate.buyer = {
					rate: body.rate,
					comment: body.comment
				}
				this.userRate(user, body.rate, true, (success) => {
					if(success)
						setRate();
				});
			}
			else if(item.buyer.equals(body.user)){
				user = item.seller;
				item.rate.seller = {
					rate: body.rate,
					comment: body.comment
				}
				this.userRate(user, body.rate, false, (success) => {
					if(success)
						setRate();
				});
			}else return cb ? cb(false, false, 'Not found User in Transaction'): false;

		 	let setRate = function() {
				this.update({_id: id},{"$set":{"rate": item.rate}}, {}, (success, mes) => {
					if(success){
						return cb ? cb(true, '¡Done!'): false;
					}else return cb ? cb(false, false, mes): false;
				})
			}.bind(this);
		}else {
			return cb ? cb(false ,false ,'Not found Transaction'): false;
		}
	})
}

method.userRate = (user, rate, type, cb) => {
	let data = {};
	User.findOne({_id: user}, (success, item) => {
		if(success){
			data = item.rating;

			switch(rate){
				case '1':
					if(type){
						data.buyer.tallied.two  += 1;
						data.buyer.total 				+= 1;
						data.buyer.totalTallied += 2;
					}else {
						data.seller.tallied.two  += 1;
						data.seller.total 				+= 1;
						data.seller.totalTallied += 2;
					}
					break;
				case '2':
					if(type){
						data.buyer.tallied.one  += 1;
						data.buyer.total				+= 1;
						data.buyer.totalTallied += 1;
					}else {
						data.seller.tallied.one  += 1;
						data.seller.total				+= 1;
						data.seller.totalTallied += 1;
					}
					break
				case '3':
					if(type){
						data.buyer.tallied.zero 	+=  1;
						if(data.buyer.total > 0)
							data.buyer.total				-=  1;
						if(data.buyer.totalTallied > 0)
							data.buyer.totalTallied -=  2;
					}else {
						data.seller.tallied.zero 	+=  1;
						if(data.seller.total > 0)
							data.seller.total				-=  1;
						if(data.seller.totalTallied > 0)
							data.seller.totalTallied -=  2;
					}
					break;
			}
			User.update({_id: user},{"$set": {rating: data}},{}, (success, mes) => {
				if(success)
					return cb ? cb(true): false;
				else return cb ? cb(false, 'Error in update User'): false;
			})
		}else return cb ? cb(false, 'Error Not found User'): false;
	})
}

module.exports = Transaction;



var Publication = require(__dirname +'/publication'),
	  Notification = require(__dirname +'/notification'),
	  User = require(__dirname +'/user'),
	  PanaPago = require(__dirname+'/panaPago'),
		Report = require(__dirname+'/report');

	User = new User();
	Publication = new Publication();
	Notification = new Notification();
	PanaPago = new PanaPago(),
	Report = new Report();
