var db = require('../utils/db'),
	collection = db.collection("notifications"),
	ObjectID = require('mongoskin').ObjectID,
	method = Notification.prototype,
	User = require(__dirname +'/user'),
	Publication = require(__dirname+'/publication'),
	Transaction = require(__dirname+'/transaction'),
	fcm = require('../utils/myFCM'),
	mailer = require('../utils/mailer');

	User = new User();
	Publication = new Publication();
	Transaction = new Transaction();

function Notification(){

}

method.update = function(query, order, extra, cb){
	collection.update(query, order, extra,  function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al actualizar") : false;
		}else{
			return cb ? cb(true) : false;
		}
	});
};

method.remove = function(filter, cb){
	collection.remove(filter, function(err){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al eliminar") : false;
		}else
			return cb ? cb(true) : false;
	});
}

method.count = function(filter, cb){
	collection.count(filter, function(err, count){
		if(err){
			console.log(err);
			cb(false, "Error al conseguir total de busqueda");
		}else
			cb(true, count);
		return;
	});
	return;
};

method.find = function(filter, order, skip, limit, cb){
	if(typeof order == 'function'){
		cb = order;
		order = false;
	}

	var promise = collection.find(filter);
	if(order)
		promise.sort(order);
	if(skip)
		promise.skip(skip);
	if(limit)
		promise.limit(limit);

	promise.toArray(function(err, users){
		if(err){
			console.log(err);
			return cb ? cb(false, "Error al ejecutar busqueda") : false;
		}else {
			if(users){
				var il = users.length, round = 1, i = 0, total = 0, newUsers = [];
				var handleLoop = function(i){
					this.process(users[i], function(success, user){
						if(success)
							newUsers[i] = user;
						else
							newUsers[i] = users[i];

						if(round == il){
							return cb ? cb(true, newUsers) : false;
						}

						round++;
						return;
					}.bind(this));
					return;
				}.bind(this);

				if(il>0)
					for(;i<il;i++)
						handleLoop(i);
				else
					return cb? cb(true, users) : false;
			}else return cb ? cb(false, "No se han encontrado notificaciones") : false;
		}
	}.bind(this));
};

method.process = function(item, cb) {
	return User.findOne({_id: item.fromUser},function(success,fromUser){
		if(success)
			item.fromUser = fromUser;
			return User.findOne({_id: item.user}, function(success,user){
				if(success)
					item.user = user;
					if(item.type === 'transaction')
						return Transaction.findOne({_id: item.meta},function(success,transaction){
							if(success)
								item.meta = transaction;
								return cb ? cb(true,item) : false;
						})
					else if(item.type === 'comment' || item.type === 'stock')
						return Publication.findOne({_id: item.meta},function(success,publication){
							if(success)
								item.meta = publication;
								return cb ? cb(true,item) : false;
						})
					else if(item.type === 'like' || item.type === 'friend' || item.type === 'friendrequest' || item.type === 'follower')
						return User.findOne({_id: item.fromUser},function(success,user){
							if(success)
								item.meta = user;
								return cb ? cb(true,item) : false;
						})
					else return cb ? cb(false) : false;
			})
	})
}
/*method.planRequest(data, cb){


	return this.insert(data, cb);
}*/

method.insert = function(data, cb){
	if(data.message &&
	data.subject &&
	ObjectID.isValid(data.user)){

		data.user = new ObjectID(data.user);
		data.fromUser = new ObjectID(data.fromUser);
		data.status = 0;
        data.time = Date.now();

		console.log(data);
		User.findOne({_id:data.user}, function(success, user){
			if(success)
				return collection.insert(data, function(err, result){
					if(err){
						return cb ? cb(false, "Error al agregar notificacion") : false;
					}else{
						if(user && user.devices.length>0){
							var tokens = [];
							for(var i = 0 ; i < user.devices.length ; i++){
								tokens.push(user.devices[i].googleToken);
							}
							fcm.send(tokens,{
								"data.body":data.message,
								"data.title":data.subject
							});
						}

						/*var mailOptions = mailer.push(user, data);
							mailOptions.to = user.mail;
							mailer.sendMail(mailOptions);*/
						return cb ? cb(true, new ObjectID(result.insertedIds[0])) : false;
					}
				});
			else
				return cb ? cb(false, "Usuario no encontrado") : false;
		}.bind(this));
	}else
		return cb ? cb(false, "Debes enviar todos los campos") : false;
};

module.exports = Notification;
