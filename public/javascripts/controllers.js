function LayoutController(GeoPosition,WebNotification,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Locality,SweetAlert,Notification) {
	$rootScope.title = $state.current.title;

	$scope.GeoPosition = angular.copy(GeoPosition);
	$scope.WebNotification = angular.copy(WebNotification);

	// $scope.WebNotification.send('hola',{});

	$scope.Notification = angular.copy(Notification);



	if(Auth.isLogged()){
		$scope.newNotification = angular.copy(Notification);
		$scope.newNotification.s.status = 1;
		$scope.newNotification.get(function(){})
		$scope.getNotificacion = function() {
			$scope.Notification.get(function(success){});
		}
		$rootScope.$watch('title',function(){
			$scope.getNotificacion();
		})
	}

	$scope.GeoPosition.request(function(success){
		console.log(success)
	});

	$rootScope.open = false;

	$scope.publications = angular.copy(Publications);
	$scope.users = angular.copy(Users);
	$scope.Locality = angular.copy(Locality);
	$scope.Auth = angular.copy(Auth)

  $scope.auth = Auth;
	if($scope.auth.isLogged()){
    $scope.auth.getUser(function(success){});
  }

  $scope.flag=false;
  $scope.index = null;
  $scope.indexx = null;

  $scope.publications.s = {
    start: 0,
    limit: 7,
    status: 1
  }
  $scope.users.s = {
    start: 0,
    limit: 7,
    status: 1
  }

	$scope.searchText = $state.params.search;

  $scope.search = function(){
    $state.go('base.search',{search: $scope.searchText,page: 1})
  }
  $scope.searchChange = function(){
		if($scope.searchText!=undefined){
			if($scope.searchText.length > 0){
				$scope.publications.items = [];
				$scope.users.items = [];
				$scope.publications.s.name = $scope.searchText;
				$scope.users.s.username = $scope.searchText;
				$scope.users.get(function(success){
					if(success){
						$scope.found=false;
						$scope.flag=true;
					}else {
						$scope.found=true;
						splice();
					}
				},false)
				$scope.publications.get(function(success){
					if(success){
						$scope.found=false;
						$scope.flag=true;
						splice();
					}else {
						$scope.found=true;
					}
				},false)
			}else {
				$scope.flag = false;
			}
			function splice(){
				if(($scope.publications.items.length > 0) && ($scope.users.items.length > 0)){
					$scope.publications.items = $scope.publications.items.splice(0,2);
					$scope.users.items = $scope.users.items.splice(0,2);
				}
			}
		}
  }
  $scope.HoverSearch = function(Item,index,flag){
      $scope.searchText = Item;
      if(flag){
        $scope.index = index;
        $scope.indexx = null;
      }else{
        $scope.indexx = index;
        $scope.index = null;
      }
  }

  $scope.Load = false;
  $scope.send = {
    form: {},
    login: function(){
      let form = this.form;
			$('#LoginCollapse').button('loading')
			$('#Login').button('loading')
      $scope.auth.login(form,function(success){
				$('#LoginCollapse').button('reset')
				$('#Login').button('reset')
        if(success)
          this.OnForm();
					$('#LoginResponsive').modal('hide')
      }.bind(this))
    },
    contact: function(){
      $http.post('http://50.21.179.35:8082/contact',this.form)
				.then(function(success){
					if(response.data.e === 0)
						console.log('Se envio el mensaje',success)
				})
    },
    register: function(){
      $scope.Load = true;
      $scope.users.a = this.form;
			$scope.users.a.document = this.form.idp+'-'+this.form.documentt;
      if(!this.form.photo){
				$scope.Load = false;
				return SweetAlert.swal({
					title:'No has incluido una imagen de perfil',
					text: 'Debes incluir un imagen de perfil para poder registrarte',
					type: 'error'
				},function(success){})
			}
			$('#RegisterBusinnes').button('loading')
			$('#Register').button('loading')
      $scope.users.post(function(success){
				$('#RegisterBusinnes').button('reset')
				$('#Register').button('reset')
        if(success){
          $scope.Load = false;
        	SweetAlert.swal({
						text: 'Registro Exitoso',
						imageUrl: 'http://50.21.179.35:8083/bill-ok.png',
					  imageWidth: 150,
					  imageHeight: 150,
					  animation: false,
						confirmButtonText:"Continuar",
						showCancelButton: true
					},function(success){
						if(success)
							$('#SignIn').modal('hide');
							$scope.send.OnForm();
					})
        }else {
          $scope.Load = false;
        }
      });
    },
    AutoFormContact: function() {
      this.form.name = $scope.auth.user.name;
      this.form.mail = $scope.auth.user.mail;
    },
    forgotpassword: function(){
			$('#Forgot').button('loading')
			$scope.Auth.forgotPassword({mail: this.form.mail},
				function(success){
					$('#Forgot').button('reset')
					SweetAlert.swal({
						title: 'Excelente',
						text: 'Sigue las instrucciones del correo electronico que te enviamos tu correo',
						imageUrl: 'http://50.21.179.35:8083/bill-ok.png',
						imageWidth: 150,
						imageHeight: 150,
						animation: false,
						confirmButtonText:"Continuar",
						showCancelButton: true
					},function(success){
						if(success)
							$('#ForgotPassword').modal('hide');
							$scope.send.OnForm();
					})
				}
			)
		},
    OnForm: function() {
      this.form = {
        username: '',
        name: '',
        mail: '',
        msj: '',
        password: '',
        lastname: '',
        phone: '',
        address: '',
        document: '',
        documentt: '',
        dob: '',
        gender: '',
        datasource: 'regular',
        priv: '3',
        photo: false,
				idp: 'V'
      };
    }
  }
  $scope.send.OnForm();
}

function IndexController($sce,GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots) {
	$rootScope.title = $state.current.title;
  $scope.users = angular.copy(Users);
  $scope.banners = angular.copy(Banners);
  $scope.category = angular.copy(Category);
  $scope.publications = angular.copy(Publications);
  $scope.spots = angular.copy(Spots);

  $scope.flagRight = true;
  $scope.flagLeft = true;
  $scope.users.s = {
    start: 0,
    limit: 5,
    status: 1
  }
  $scope.banners.get(function(success){
    if(success){

    }
  })
  $scope.category.s = {
    parent: "false"
  }
  $scope.category.get(function(success){
    if(success){
      $scope.ParentCat = $scope.category.items;
      $scope.categorySingle($scope.ParentCat[0]._id);
    }
  })
  $scope.categorySingle = function(Id){
    if(Id){
      $scope.category.s = {
          parent: Id
      }
      GetCategory();

    }else {
      $scope.category.s = {
        parent: Id
      }
      GetCategory();
    }
    function GetCategory(){
      $scope.category.get(function(success){
        if(success){
          $scope.listCat = $scope.category.items;
          $scope.active = $scope.category.s.parent;
        }
      })
    }
  }
  $scope.publications.get(function(success){
    if(success){
    }
  })
  $scope.users.get(function(success) {
    if(success){

    }
  })
	$scope.trustSrc = function(src) {
		url = 'https://www.youtube.com/embed/'+src+'?rel=0';
    return $sce.trustAsResourceUrl(url);
  }
	$scope.array = [];
  $scope.spots.get(function(success){
		if(success){
			if($scope.spots.total>1){
				$scope.pages = (Math.ceil($scope.spots.total/3));
				for(var i=0;i<$scope.pages;i++){
					$scope.array[i] = [];

					var y = 0;
					y = i*3;

					if(i==0){
						$scope.array[i][0] = $scope.spots.items[y];
						$scope.array[i][1] = $scope.spots.items[y+1];
						$scope.array[i][2] = $scope.spots.items[y+2];
					}
					if(i>0){
						$scope.array[i][0] = $scope.spots.items[y];
						$scope.array[i][1] = $scope.spots.items[y+1];
						$scope.array[i][2] = $scope.spots.items[y+2];
					}
				}
			}else {
				$scope.pages = 1;
			}
		}
	})
}

function SummaryController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.toanswer = angular.copy(Comments);
	$scope.toanswer.s = {
		toMe: true,
		start: 0,
		limit: 1,
	};
	$scope.toanswer.get();

	$scope.answered = angular.copy(Comments);
	$scope.answered.s = {
		byMe: true,
		start: 0,
		limit: 1,
		// answered: 'false'
	};
	$scope.answered.get();


	$scope.publiActive = angular.copy(Publications);
	$scope.publiActive.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.publiActive.get();

	$scope.publiPaused = angular.copy(Publications);
	$scope.publiPaused.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.publiPaused.get();
	$scope.publiEnded = angular.copy(Publications);
	$scope.publiEnded.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 3
	};
	$scope.publiEnded.get();
	$scope.publiShared = angular.copy(Publications);
	$scope.publiShared.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 31
	};
	$scope.publiShared.get();

	$scope.completeSales = angular.copy(Transactions);
	$scope.completeSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		statuss: 2,
		statusb: 2
	};
	$scope.completeSales.get();

	$scope.activesSales = angular.copy(Transactions);
	$scope.activesSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		statuss: 1,
		statusb: 1
	};
	$scope.activesSales.get();

	$scope.completePurchases = angular.copy(Transactions);
	$scope.completePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		statusb: 2,
		statuss: 2
	};
	$scope.completePurchases.get();

	$scope.activePurchases = angular.copy(Transactions);
	$scope.activePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		statusb: 1,
		statuss: 1
	};
	$scope.activePurchases.get();

}

function salesPublicationsController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots,SweetAlert){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$rootScope.salesActive = true;

	$scope.itemsActive = [];
	$scope.itemsPaused = [];
	$scope.itemsEnded = [];
	$scope.totalActive = 1000;
	$scope.totalPaused = 1000;
	$scope.totalEnded = 1000;
	$scope.limit = 10;
	$scope.flag = false;

	$scope.publiActive 	= angular.copy(Publications);
	$scope.publiPaused 	= angular.copy(Publications);
	$scope.publiEnded 	= angular.copy(Publications);
	$scope.publications = angular.copy(Publications);

	$scope.state = $stateParams.state;

	$scope.publiActive.s = {
		byMe: true,
		limit: $scope.limit,
		status: 1,
		// orderDate: 1
	};

	$scope.publiPaused.s = {
		byMe: true,
		limit: $scope.limit,
		status: 2,
		// orderDate: -1
	};

	$scope.publiEnded.s = {
		byMe: true,
		limit: $scope.limit,
		status: 3,
		// orderDate: -1
	};

	$scope.send = {
		form: {},
		search: function(){
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(options){
			if(options){
				this.form.filter = options;
				$state.go('.',{orderSales: this.form.filter});
			}else {
				$state.go('.',{orderDate: this.form.sort});
			}
		},
		OnModalPublication: function(id){
			let thiss = this;
			$scope.publications.getSingle(id,function(success){
				if(success){
					let data = angular.copy($scope.publications.item);
					thiss.form._id 		= data._id;
					thiss.form.name 	= data.name;
					thiss.form.stock 	= data.stock;
					thiss.form.price 	= data.price;
					$('#PublicationEdit').modal('show')
				}
			},false)
		},
		onSubmit: function(){
			let data = {};

			if(this.form.name != $scope.publications.item.name)
				data.name = this.form.name;
			if(this.form.stock != $scope.publications.item.stock)
				data.stock = this.form.stock;
			if(this.form.price != $scope.publications.item.price)
				data.price = this.form.price;

			data._id = this.form._id;

			if(data.name || data.stock || data.price){
				$scope.publications.p = data;
				$('#CancelModalPublicationEdit').hide()
				$scope.publications.put(function(success){
					$('#ButtonModalPublicationEdit').button('loading')
					if(success){
						$('#ButtonModalPublicationEdit').button('reset')
						$('#CancelModalPublicationEdit').show()
						$('#PublicationEdit').modal('hide')
						SweetAlert.swal({
							title: 'Exito',
							text: 'Se ha modificado tu publicacion',
							imageUrl: 'http://50.21.179.35:8083/bill-ok.png',
						  imageWidth: 150,
						  imageHeight: 150,
						  animation: false,
							confirmButtonText:"Continuar",
							showCancelButton: true
						})
						$scope.publiActive.get(function(success){
							if(success){
								$scope.itemsActive = $scope.publiActive.items;
								$scope.totalActive = $scope.publiActive.total;
								if($scope.totalActive>0)
									$scope.flag = true;
							}
						});
					}
				})
			}else {
				$('#alertPublicationEdit').show()
			}
		},
		redirection: function(id){
			$('#PublicationEdit').modal('hide')
			$timeout(function(){
				$state.go("summary.publicationEdit",{id: id})
			},500)
		},
		ChangeStatus: function(id,status){
			$scope.publications.p = {
				_id: id,
				status: status
			}
			$scope.publications.put(function(success){
				if(success){
					OnLoadCount()
				}
			})
		}
	}

	$('#PublicationEdit').on('hidden.bs.modal', function(){
		$('#ButtonModalPublicationEdit').button('reset')
		$('#CancelModalPublicationEdit').show()
		$('#alertPublicationEdit').addClass('hidden')
		$scope.send.form = {};
	})

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};
	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	}

	function OnLoadCount(){
		switch ($stateParams.state) {
			case 'enabled':
				$scope.options = 0;
				console.log('enable')
				$scope.publiActive.s.name 			= $stateParams.search ? $stateParams.search: '';
				$scope.currentPage 							= $stateParams.page ? $stateParams.page: 1;
				$scope.publiActive.s.start 			= $scope.limit*($scope.currentPage-1);
				if($stateParams.orderDate)
					$scope.publiActive.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
					$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
				if($stateParams.orderSales)
					$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
					$scope.publiActive.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.publiActive.get(function(success){
					if(success){
						$scope.itemsActive = $scope.publiActive.items;
						$scope.totalActive = $scope.publiActive.total;
						if($scope.totalActive>0)
							$scope.flag = true;
					}
				});
				break;
			case 'paused':
				$scope.options = 1;
				console.log('paused')
				$scope.publiPaused.s.name 			= $stateParams.search ? $stateParams.search: '';
				$scope.currentPage 							= $stateParams.page ? $stateParams.page: 1;
				$scope.publiPaused.s.start 			= $scope.limit*($scope.currentPage-1);
				if($stateParams.orderDate)
					$scope.publiPaused.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
					$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
				if($stateParams.orderSales)
					$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
					$scope.publiPaused.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.publiPaused.get(function(success){
					if(success){
						$scope.itemsPaused = $scope.publiPaused.items;
						$scope.totalPaused = $scope.publiPaused.total;
						if($scope.totalPaused>0)
							$scope.flag = true;
					}
				});
				break;
			case 'ended':
				$scope.options = 2;
				console.log('ended')
				$scope.publiEnded.s.name 				= $stateParams.search ? $stateParams.search: '';
				$scope.currentPage 							= $stateParams.page ? $stateParams.page: 1;
				$scope.publiEnded.s.start 			= $scope.limit*($scope.currentPage-1);
				if($stateParams.orderDate)
					$scope.publiEnded.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
					$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
				if($stateParams.orderSales)
					$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
					$scope.publiEnded.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.publiEnded.get(function(success){
					if(success){
						$scope.itemsEnded = $scope.publiEnded.items;
						$scope.totalEnded = $scope.publiEnded.total;
						if($scope.totalEnded>0)
							$scope.flag = true;
					}
				});
				break;
			default:
				console.log('default')
				if($stateParams.search){
					$scope.publiActive.s.name = $stateParams.search;
				}
				$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
				$scope.publiActive.s.start = $scope.limit*($scope.currentPage-1);
				$scope.publiActive.get(function(success){
					if(success){
						$scope.itemsActive = $scope.publiActive.items;
						$scope.totalActive = $scope.publiActive.total;
						if($scope.totalActive>0)
							$scope.flag = true;
					}
				});
		}

		$scope.publiActiveTotal = angular.copy(Publications);
		$scope.publiActiveTotal.s = {
			byMe: true,
			status: 1,
			limit: 1
		};
		$scope.publiActiveTotal.get(function(success){
			$scope.activeTotal = $scope.publiActiveTotal.total;
		});
		$scope.publiPausedTotal = angular.copy(Publications);
		$scope.publiPausedTotal.s = {
			byMe: true,
			status: 2,
			limit: 1
		};
		$scope.publiPausedTotal.get(function(success){
			$scope.pauseTotal = $scope.publiPausedTotal.total;
		});
		$scope.publiEndedTotal = angular.copy(Publications);
		$scope.publiEndedTotal.s = {
			byMe: true,
			status: 3,
			limit: 1
		};
		$scope.publiEndedTotal.get(function(success){
			$scope.endedTotal = $scope.publiEndedTotal.total;
		});
	}

	OnLoadCount()

	if($stateParams.search){
		$scope.send.form.search = $stateParams.search;
	}
}

function salesSalesController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots,SweetAlert,Reports,Rate){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;
	$rootScope.salesActive = true;

	$scope.itemsActive = [];
	$scope.itemsComplete = [];
	$scope.totalActive = 1000;
	$scope.totalComplete = 1000;
	$scope.limit = 10;
	$scope.flag = false;

	$scope.activesSales = angular.copy(Transactions);
	$scope.completeSales = angular.copy(Transactions);
	$scope.reports = angular.copy(Reports);
	$scope.rate = angular.copy(Rate);

	$scope.activesSales.s = {
		sales: true,
		start: 0,
		limit: $scope.limit,
		statuss: 1,
		statusb: 1
	};
	$scope.completeSales.s = {
		sales: true,
		start: 0,
		limit: $scope.limit,
		statuss: 2,
		statusb: 2
	};

	$scope.send = {
		form: {},
		search: function(){
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(options){
			if(options){
				this.form.filter = options;
				$state.go('.',{orderSales: this.form.filter});
			}else {
				$state.go('.',{orderDate: this.form.sort});
			}
		}
	}

	$scope.state = $stateParams.state;
	switch ($stateParams.state) {
		case 'enabled':
			$scope.options = 0;
			console.log('enable')
			$scope.activesSales.s.name = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.activesSales.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.activesSales.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.activesSales.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.activesSales.get(function(success){
				if(success){
					$scope.itemsActive = $scope.activesSales.items;
					$scope.totalActive = $scope.activesSales.total;
					if($scope.totalActive>0)
						$scope.flag = true;
				}
			});
			break;
		case 'complete':
			$scope.options = 1;
			console.log('complete')
			$scope.completeSales.s.name = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.completeSales.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.completeSales.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.completeSales.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.completeSales.get(function(success){
				if(success){
					$scope.itemsComplete = $scope.completeSales.items;
					$scope.totalComplete = $scope.completeSales.total;
					if($scope.totalComplete>0)
						$scope.flag = true;
				}
			});
			break;
		default:
			$scope.options = 0;
			console.log('enable')
			$scope.activesSales.s.name = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.activesSales.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.activesSales.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.activesSales.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.activesSales.get(function(success){
				if(success){
					$scope.itemsActive = $scope.activesSales.items;
					$scope.totalActive = $scope.activesSales.total;
					if($scope.totalActive>0)
						$scope.flag = true;
				}
			});
	}

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};
	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.viewBuyer = function($index){
		if($stateParams.state=='enabled')
			$scope.viewTrans = $scope.itemsActive[$index];
		if($stateParams.state=='complete')
			$scope.viewTrans = $scope.itemsComplete[$index];
		$('#viewBuyer').modal('show')
	}

	$scope.activesSalesTotal = angular.copy(Transactions);
	$scope.activesSalesTotal.s = {
		sales: true,
		start: 0,
		limit: 1,
		statuss: 1
	};
	$scope.activesSalesTotal.get(function(success){
		$scope.activeTotal = $scope.activesSalesTotal.total;
	});
	$scope.completeSalesTotal = angular.copy(Transactions);
	$scope.completeSalesTotal.s = {
		sales: true,
		start: 0,
		limit: 1,
		statuss: 2
	};
	$scope.completeSalesTotal.get(function(success){
		$scope.endedTotal = $scope.completeSalesTotal.total;
	});

	$scope.active = 0;

	$scope.passedTransaction = function(item){
		$scope.passed = item;
		$scope.passed.cancelPrice = 0;
		$scope.passed.remainingPrice = item.price;
		for (var items of item.payments){
			$scope.passed.cancelPrice += parseInt(items.sum);
			if($scope.passed.remainingPrice > 0)
				$scope.passed.remainingPrice -= parseInt(items.sum);
		}
		console.log($scope.passed)
	}
	$scope.deliveryTransaction = function(item){
		if(item.delivery)
			$scope.delivery = item.delivery;
		else
			$scope.delivery = false;
		console.log($scope.delivery)
	}
	$scope.deliveryTransactionAlert = function(){
		return SweetAlert.swal({
			title:'',
			text: 'Aún no has realizado ningun envio.',
			type: 'warning'
		},function(success){})
	}
	$scope.ratingTransaction = function(item){
		$scope.rating = item;
		$scope.on.form = item.rate.seller ? item.rate.seller: {};
		$scope.on.form._id = item._id;
		console.log($scope.on.form);
	}
	$scope.claimsTransaction = function(item){
		$scope.claims = item;
	}
	$scope.on = {
		form: {},
		delivery: function(){

		},
		claims: function(){
			$('#btn-claims').button('loading');
			this.form.transaction = $scope.claims._id;
			this.form.by = $scope.claims.buyer._id;

			$scope.reports.a = this.form;
			$scope.reports.a._id = $scope.claims._id;
			$scope.reports.post(function(success){
				$('#btn-claims').button('reset');
				if(success){
					$('#claims').modal('hide');
					SweetAlert.swal({
						title: 'Exito',
						text: 'Tu reclamo ha sido enviado',
						imageUrl: 'http://50.21.179.35:8083/bill-ok.png',
						imageWidth: 150,
						imageHeight: 150,
						animation: false,
						confirmButtonText:"Continuar",
						showCancelButton: true,
					},function(success){});
				}
			});
		},
		rating: function(){
			if(this.form.rate === undefined)
				return;
			if(this.form.comment == undefined)
				return;
			$scope.rate.a = this.form;
			$scope.rate.post((success) => {
				if(success)
					console.log(success)
			})
		}
	}

	$scope.redirection = function(item){
		$('#purchase').modal('hide')
		$timeout(function(){
			$state.go('base.checkout',{id: item._id});
		},500)
	}

	if($stateParams.search){
		$scope.send.form.search = $stateParams.search;
	}

	$scope.banksaccount = [
			{
				name: 'Venezuela',
				value: 1
			},
			{
				name: 'Banesco',
				value: 2
			},
			{
				name: 'Provincial',
				value: 3
			},
			{
				name: 'Mercantil',
				value: 4
			},
			{
				name: 'BOD',
				value: 5
			},
			{
				name: 'Bicentenario',
				value: 6
			},
			{
				name: 'Del Tesoro',
				value: 7
			},
			{
				name: 'Bancaribe',
				value: 8
			},
			{
				name: 'Exterior',
				value: 9
			},
			{
				name: 'BNC',
				valule: 10
			},
			{
				name: 'BFC',
				value: 11
			},
			{
				name: 'BVC',
				value: 12
			},
			{
				name: 'Industrial',
				value: 13
			},
			{
				name: 'Caroní',
				value: 14
			},
			{
				name: 'Sofitasa',
				value: 15
			},
			{
				name: 'Banplus',
				value: 16
			},
			{
				name: 'Plaza',
				value: 17
			},
			{
				name: 'Activo',
				value: 18
			},
			{
				name: 'Bancrecer',
				value: 19
			},
			{
				name: 'Del Sur',
				value: 20
			},
			{
				name: '100% Banco',
				value: 21
			},
			{
				name: 'Agricola',
				value: 22
			},
			{
				name: 'Citibank',
				value: 23
			},
			{
				name: 'BANFANB',
				value: 24
			},
			{
				name: 'Bancamiga',
				value: 25
			},
			{
				name: 'Bangente',
				value: 26
			},
			{
				name: 'Mi Banco',
				value: 27
			},
			{
				name: 'Novo Banco',
				value: 28
			}
		]

}

function salesQuestionController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;
	$rootScope.salesActive = true;

	$scope.answered = angular.copy(Comments);
	$scope.comments = angular.copy(Comments);
	$scope.answered.s = {
		toMe: true,
		start: 0,
	};
	$scope.answered.get();

	$scope.send = {
		form: {},
		comments: function(id,publication){
			$scope.comments.a = this.form;
			$scope.comments.a.parent = id;
			$scope.comments.a.publication = publication;
			$scope.comments.post(function(success){
				if(success)
					$scope.send.form = {};
					$scope.answered.get();
			})
		}
	}

	let letter = 0;
	$scope.letter = {
		count: 240,
		on: function(){
			if($scope.send.form.comment.length>letter)
				this.count = this.count - 1;
			else
				this.count = this.count + 1;

			letter = $scope.send.form.comment.length;
		}
	}

	$scope.show = function(index){
		console.log(index)
		$('#'+index+' .show').addClass('hidden');
		$('#'+index+' .hiden').removeClass('hidden');
		$('#box-comments'+index).addClass('in');
	}
	$scope.hide = function(index){
		console.log(index)
		$('#'+index+' .show').removeClass('hidden');
		$('#'+index+' .hiden').addClass('hidden');
		$('#box-comments'+index).removeClass('in');
	}

}

function salesClaimsController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;
	$rootScope.salesActive = true;

	$scope.toanswer = angular.copy(Comments);
	$scope.toanswer.s = {
		toMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.toanswer.get();

	$scope.answered = angular.copy(Comments);
	$scope.answered.s = {
		byMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.answered.get();


	$scope.publiActive = angular.copy(Publications);
	$scope.publiActive.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.publiActive.get();

	$scope.publiPaused = angular.copy(Publications);
	$scope.publiPaused.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.publiPaused.get();
	$scope.publiEnded = angular.copy(Publications);
	$scope.publiEnded.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 3
	};
	$scope.publiEnded.get();
	$scope.publiShared = angular.copy(Publications);
	$scope.publiShared.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 31
	};
	$scope.publiShared.get();

	$scope.completeSales = angular.copy(Transactions);
	$scope.completeSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completeSales.get();

	$scope.activesSales = angular.copy(Transactions);
	$scope.activesSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activesSales.get();

	$scope.completePurchases = angular.copy(Transactions);
	$scope.completePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completePurchases.get();

	$scope.activePurchases = angular.copy(Transactions);
	$scope.activePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activePurchases.get();

}

function purchasesFavoritesController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;
	$scope.publications = angular.copy(Publications);

	$scope.items = [];
	$scope.total = 1000;
	$scope.limit = 2;
	$scope.flag = false;

	$scope.publications.s = {
		byMe: true,
		start: 0,
		limit: $scope.limit,
		status: 1
	};

	$scope.publications.s.search = $stateParams.search ? $stateParams.search: '';
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.publications.s.start = $scope.limit*($scope.currentPage-1);

	if($stateParams.orderDate)
		$scope.publications.s.orderDate = $stateParams.orderDate ? $stateParams.orderDate: '';
		$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
	if($stateParams.orderSales)
		$scope.send.form.sort = $stateParams.orderSales ? $stateParams.orderSales: '';
		$scope.publications.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';

	$scope.publications.get(function(success){
		if(success)
			$scope.items = $scope.publications.items;
			$scope.total = $scope.publications.total;
			if($scope.total>0)
				$scope.flag = true;
	});

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};
	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.send = {
		form: {},
		search: function(){
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(){
			console.log(this.form.sort)
			switch(this.form.sort){
				case '0':
						$state.go('.',{orderSales: -1,orderDate: undefined});
					break;
				case '1':
						$state.go('.',{orderSales: 1,orderDate: undefined});
					break;
				case '2':
						$state.go('.',{orderDate: -1,orderSales: undefined});
					break;
				case '3':
						$state.go('.',{orderDate: 1,orderSales: undefined});
					break;
			}
		}
	}

	if($stateParams.search){
		$scope.send.form.search = $stateParams.search;
	}

}

function purchasesQuestionController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
	if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.answered = angular.copy(Comments);
	$scope.comments = angular.copy(Comments);
	$scope.answered.s = {
		byMe: true,
		start: 0,
	};
	$scope.answered.get();

	$scope.send = {
		form: {},
		comments: function(id,publication){
			$scope.comments.a = this.form;
			$scope.comments.a.parent = id;
			$scope.comments.a.publication = publication;
			$scope.comments.post(function(success){
				if(success)
					$scope.send.form = {};
					$scope.answered.get();
			})
		}
	}

	let letter = 0;
	$scope.letter = {
		count: 240,
		on: function(){
			if($scope.send.form.comment.length>letter)
				this.count = this.count - 1;
			else
				this.count = this.count + 1;

			letter = $scope.send.form.comment.length;
		}
	}

	$scope.show = function(index){
		console.log(index)
		$('#'+index+' .show').addClass('hidden');
		$('#'+index+' .hiden').removeClass('hidden');
		$('#box-comments'+index).addClass('in');
	}
	$scope.hide = function(index){
		console.log(index)
		$('#'+index+' .show').removeClass('hidden');
		$('#'+index+' .hiden').addClass('hidden');
		$('#box-comments'+index).removeClass('in');
	}

}

function purchasesPurchasesController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots,SweetAlert,Reports,Rate){
	if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.itemsActive = [];
	$scope.itemsComplete = [];
	$scope.totalActive = 1000;
	$scope.totalComplete = 1000;
	$scope.limit = 10;
	$scope.flag = false;

	$scope.activesPurchases = angular.copy(Transactions);
	$scope.completePurchases = angular.copy(Transactions);
	$scope.reports = angular.copy(Reports);
	$scope.rate = angular.copy(Rate);

	$scope.activesPurchases.s = {
		purchases: true,
		start: 0,
		limit: $scope.limit,
		statuss: 1,
		statusb: 1
	};

	$scope.completePurchases.s = {
		purchases: true,
		start: 0,
		limit: $scope.limit,
		statuss: 2,
		statusb: 2
	};

	$scope.send = {
		form: {},
		search: function(){
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(options){
			if(options){
				this.form.filter = options;
				$state.go('.',{orderSales: this.form.filter});
			}else {
				$state.go('.',{orderDate: this.form.sort});
			}
		}
	}

	$scope.state = $stateParams.state;
	switch ($stateParams.state) {
		case 'enabled':
			$scope.options = 0;
			console.log('enable')
			$scope.activesPurchases.s.search = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.activesPurchases.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.activesPurchases.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.activesPurchases.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.activesPurchases.get(function(success){
				if(success){
					$scope.itemsActive = $scope.activesPurchases.items;
					$scope.totalActive = $scope.activesPurchases.total;
					if($scope.totalActive>0)
						$scope.flag = true;
				}
			});
			break;
		case 'complete':
			$scope.options = 1;
			console.log('paused')
			$scope.completePurchases.s.search = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.completePurchases.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.completePurchases.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.completePurchases.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.completePurchases.get(function(success){
				if(success){
					$scope.itemsComplete = $scope.completePurchases.items;
					$scope.totalComplete = $scope.completePurchases.total;
					if($scope.totalComplete>0)
						$scope.flag = true;
				}
			});
			break;
		default:
			$scope.options = 0;
			$scope.activesPurchases.s.search = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.activesPurchases.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.activesPurchases.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.activesPurchases.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.activesPurchases.get(function(success){
				if(success){
					$scope.itemsActive = $scope.activesPurchases.items;
					$scope.totalActive = $scope.activesPurchases.total;
					if($scope.totalActive)
						$scope.flag = true;
				}
			});
	}

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};
	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.activesPurchasesTotal = angular.copy(Transactions);
	$scope.activesPurchasesTotal.s = {
		purchases: true,
		statuss: 1,
		limit: 1
	};
	$scope.activesPurchasesTotal.get(function(success){
		$scope.activeTotal = $scope.activesPurchasesTotal.total;
	});
	$scope.completePurchasesTotal = angular.copy(Transactions);
	$scope.completePurchasesTotal.s = {
		purchases: true,
		statuss: 2,
		limit: 1
	};
	$scope.completePurchasesTotal.get(function(success){
		$scope.endedTotal = $scope.completePurchasesTotal.total;
	});

	$scope.active = 0;

	$scope.passedTransaction = function(item){
		$scope.passed = item;
		$scope.passed.cancelPrice = 0;
		$scope.passed.remainingPrice = item.price;
		for (var items of item.payments){
			$scope.passed.cancelPrice += parseInt(items.sum);
			if($scope.passed.remainingPrice > 0)
				$scope.passed.remainingPrice -= parseInt(items.sum);
		}
		console.log($scope.passed)
	}
	$scope.deliveryTransaction = function(item){
		if(item.delivery)
			$scope.delivery = item.delivery;
		else
			$scope.delivery = false;
		console.log($scope.delivery)
	}
	$scope.deliveryTransactionAlert = function(){
		return SweetAlert.swal({
			title:'',
			text: 'Aún no te han realizado el envio',
			type: 'warning'
		},function(success){})
	}
	$scope.ratingTransaction = function(item){
		$scope.rating = item;
		$scope.on.form = item.rate.seller ? item.rate.seller: {};
		$scope.on.form._id = item._id;
		console.log($scope.on.form);
	}
	$scope.claimsTransaction = function(item){
		$scope.claims = item;
	}
	$scope.on = {
		form: {},
		delivery: function(){

		},
		claims: function(){
			$('#btn-claims').button('loading');
			this.form.transaction = $scope.claims._id;
			this.form.by = $scope.claims.buyer._id;

			$scope.reports.a = this.form;
			$scope.reports.a._id = $scope.claims._id;
			$scope.reports.post(function(success){
				$('#btn-claims').button('reset');
				if(success){
					$('#claims').modal('hide');
					SweetAlert.swal({
						title: 'Exito',
						text: 'Tu reclamo ha sido enviado',
						imageUrl: 'http://50.21.179.35:8083/bill-ok.png',
						imageWidth: 150,
						imageHeight: 150,
						animation: false,
						confirmButtonText:"Continuar",
						showCancelButton: true,
					},function(success){});
				}
			});
		},
		rating: function(){
			if(this.form.rate === undefined)
				return;
			if(this.form.comment == undefined)
				return;
			$scope.rate.a = this.form;
			$scope.rate.post((success) => {
				if(success)
					console.log(success)
			})
		}
	}

	$scope.redirection = function(item){
		$('#purchase').modal('hide')
		$timeout(function(){
			$state.go('base.checkout',{id: item._id});
		},500)
	}

	if($stateParams.search){
		$scope.send.form.search = $stateParams.search;
	}

	$scope.banksaccount = [
			{
				name: 'Venezuela',
				value: 1
			},
			{
				name: 'Banesco',
				value: 2
			},
			{
				name: 'Provincial',
				value: 3
			},
			{
				name: 'Mercantil',
				value: 4
			},
			{
				name: 'BOD',
				value: 5
			},
			{
				name: 'Bicentenario',
				value: 6
			},
			{
				name: 'Del Tesoro',
				value: 7
			},
			{
				name: 'Bancaribe',
				value: 8
			},
			{
				name: 'Exterior',
				value: 9
			},
			{
				name: 'BNC',
				valule: 10
			},
			{
				name: 'BFC',
				value: 11
			},
			{
				name: 'BVC',
				value: 12
			},
			{
				name: 'Industrial',
				value: 13
			},
			{
				name: 'Caroní',
				value: 14
			},
			{
				name: 'Sofitasa',
				value: 15
			},
			{
				name: 'Banplus',
				value: 16
			},
			{
				name: 'Plaza',
				value: 17
			},
			{
				name: 'Activo',
				value: 18
			},
			{
				name: 'Bancrecer',
				value: 19
			},
			{
				name: 'Del Sur',
				value: 20
			},
			{
				name: '100% Banco',
				value: 21
			},
			{
				name: 'Agricola',
				value: 22
			},
			{
				name: 'Citibank',
				value: 23
			},
			{
				name: 'BANFANB',
				value: 24
			},
			{
				name: 'Bancamiga',
				value: 25
			},
			{
				name: 'Bangente',
				value: 26
			},
			{
				name: 'Mi Banco',
				value: 27
			},
			{
				name: 'Novo Banco',
				value: 28
			}
		]
}

function billingPayController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.toanswer = angular.copy(Comments);
	$scope.toanswer.s = {
		toMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.toanswer.get();

	$scope.answered = angular.copy(Comments);
	$scope.answered.s = {
		byMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.answered.get();


	$scope.publiActive = angular.copy(Publications);
	$scope.publiActive.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.publiActive.get();

	$scope.publiPaused = angular.copy(Publications);
	$scope.publiPaused.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.publiPaused.get();
	$scope.publiEnded = angular.copy(Publications);
	$scope.publiEnded.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 3
	};
	$scope.publiEnded.get();
	$scope.publiShared = angular.copy(Publications);
	$scope.publiShared.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 31
	};
	$scope.publiShared.get();

	$scope.completeSales = angular.copy(Transactions);
	$scope.completeSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completeSales.get();

	$scope.activesSales = angular.copy(Transactions);
	$scope.activesSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activesSales.get();

	$scope.completePurchases = angular.copy(Transactions);
	$scope.completePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completePurchases.get();

	$scope.activePurchases = angular.copy(Transactions);
	$scope.activePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activePurchases.get();

}

function managerNetworkLinkController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.toanswer = angular.copy(Comments);
	$scope.toanswer.s = {
		toMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.toanswer.get();

	$scope.answered = angular.copy(Comments);
	$scope.answered.s = {
		byMe: true,
		start: 0,
		limit: 1,
		answered: 'false'
	};
	$scope.answered.get();


	$scope.publiActive = angular.copy(Publications);
	$scope.publiActive.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.publiActive.get();

	$scope.publiPaused = angular.copy(Publications);
	$scope.publiPaused.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.publiPaused.get();
	$scope.publiEnded = angular.copy(Publications);
	$scope.publiEnded.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 3
	};
	$scope.publiEnded.get();
	$scope.publiShared = angular.copy(Publications);
	$scope.publiShared.s = {
		byMe: true,
		start: 0,
		limit: 1,
		status: 31
	};
	$scope.publiShared.get();

	$scope.completeSales = angular.copy(Transactions);
	$scope.completeSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completeSales.get();

	$scope.activesSales = angular.copy(Transactions);
	$scope.activesSales.s = {
		sales: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activesSales.get();

	$scope.completePurchases = angular.copy(Transactions);
	$scope.completePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 2
	};
	$scope.completePurchases.get();

	$scope.activePurchases = angular.copy(Transactions);
	$scope.activePurchases.s = {
		purchases: true,
		start: 0,
		limit: 1,
		status: 1
	};
	$scope.activePurchases.get();

}

function managerNetworkPublicationAutoController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.itemsActive = [];
	$scope.itemsComplete = [];
	$scope.totalActive = 1000;
	$scope.totalPaused = 1000;
	$scope.limit = 2;
	$scope.flag = false;

	$scope.activePublication = angular.copy(Publications);
	$scope.pausedPublication = angular.copy(Publications);

	$scope.activePublication.s = {
		byMe: true,
		start: 0,
		limit: $scope.limit,
		status: 31
	};

	$scope.pausedPublication.s = {
		byMe: true,
		start: 0,
		limit: $scope.limit,
		status: 31
	};

	$scope.send = {
		form: {},
		search: function(){
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(options){
			if(options){
				this.form.filter = options;
				$state.go('.',{orderSales: this.form.filter});
			}else {
				$state.go('.',{orderDate: this.form.sort});
			}
		}
	}

	$scope.state = $stateParams.state;
	switch ($stateParams.state) {
		case 'enabled':
			$scope.options = 0;
			console.log('enable')
			$scope.activePublication.s.search = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.activePublication.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.activePublication.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.activePublication.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.activePublication.get(function(success){
				if(success){
					$scope.itemsActive = $scope.activePublication.items;
					$scope.totalActive = $scope.activePublication.total;
					if($scope.totalActive>0)
						$scope.flag = true;
				}
			});
			break;
		case 'paused':
			$scope.options = 1;
			console.log('paused')
			$scope.pausedPublication.s.search = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.pausedPublication.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.pausedPublication.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.pausedPublication.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.pausedPublication.get(function(success){
				if(success){
					$scope.itemsPaused = $scope.pausedPublication.items;
					$scope.totalPaused = $scope.pausedPublication.total;
					if($scope.totalPaused>0)
						$scope.flag = true;
				}
			});
			break;
		default:
			$scope.options = 0;
			$scope.activePublication.s.search = $stateParams.search ? $stateParams.search: '';
			$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
			$scope.activePublication.s.start = $scope.limit*($scope.currentPage-1);
			if($stateParams.orderDate)
				$scope.activePublication.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
				$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
			if($stateParams.orderSales)
				$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
				$scope.activePublication.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
			$scope.activePublication.get(function(success){
				if(success){
					$scope.itemsActive = $scope.activePublication.items;
					$scope.totalActive = $scope.activePublication.total;
					if($scope.totalActive)
						$scope.flag = true;
				}
			});
	}


	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};
	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.activePublicationTotal = angular.copy(Publications);
	$scope.activePublicationTotal.s = {
		byMe: true,
		status: 31
	};
	$scope.activePublicationTotal.get(function(success){
		$scope.activeTotal = $scope.activePublicationTotal.total;
	});
	$scope.pausedPublicationTotal = angular.copy(Publications);
	$scope.pausedPublicationTotal.s = {
		byMe: true,
		status: 31
	};
	$scope.pausedPublicationTotal.get(function(success){
		$scope.pausedTotal = $scope.pausedPublicationTotal.total;
	});

	$scope.active = 0;

	if($stateParams.search){
		$scope.send.form.search = $stateParams.search;
	}

}

function managerNetworkCampaignsController(Transactions, Comments, GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Banners,Category,Publications,Users,Spots){
  if(!Auth.isLogged())
		return $state.go('base.index');
  $rootScope.title = $state.current.title;

	$scope.Auth = Auth;

	$scope.send = {
		form: {},
		on: function(){

		},
		delete: function(index){
			console.log(index)
			console.log($scope.files)
		}
	}

	// $scope.days = ['Domingo','Lunes','Martes','Miercoles','Jueves','Sabado'];
	// $scope.dayShourt = ['Do','Lu','Ma','Mi','Ju','Vi','Sa'];
	// $scope.Arraydays = [];
  //
	// $scope.pushDays = function() {
	// 	$scope.send.form.days = [];
	// 	for(i=0;i<$scope.Arraydays.length;i++){
	// 		$scope.send.form.days[i] = $scope.dayShourt[$scope.Arraydays[i]];
	// 	}
	// }

	let letter = 0;
  $scope.letter = {
    count: 240,
    on: function(){
      if($scope.send.form.comment.length>letter)
        this.count = this.count - 1;
      else
        this.count = this.count + 1;

      letter = $scope.send.form.comment.length;
    }
  }

}

function configurationDataUserController(Transactions,GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,SweetAlert,Auth,Users,Banks,BanksAccount){
	$rootScope.title = $state.current.title;

	if(!Auth.isLogged())
		return $state.go('base.index');

	$scope.users 				= angular.copy(Users)
	$scope.banks 				= angular.copy(Banks);
	$scope.banksaccount = angular.copy(BanksAccount);

	$scope.banks.get()

	$scope.send = {
		form: {},
		EditInfoPerson: function(){
			$('#CancelModal').hide()
			$('#ButtonModal').button('loading')
			let data = {},
					document = $scope.Auth.document.split("-");
			if(this.form.username != $scope.Auth.username)
				data.username = this.form.username;
			if(this.form.document != document[1] || this.form.idp != document[0])
				data.document = this.form.idp+'-'+this.form.document;
			if(this.form.phone != $scope.Auth.phone)
			 	data.phone = this.form.phone;
			if(this.form.address != $scope.Auth.address)
				data.address = this.form.address;
			if(Array.isArray(this.form.localities))
				if(Array.isArray($scope.Auth.localities)){
					for(var e in this.form.localities)
						if(this.form.localities[e] != $scope.Auth.localities[e]){
							data.localities[e] = this.form.localities[e] ? this.form.localities[e]: '';
							console.log(data.localities[e])
						}
				}else {
					data.localities = this.form.localities;
				}
			if(data.username || data.document || data.phone || data.address || data.localities){
				$('#ButtonModal').button('loading')
				$scope.users.p = data;
				$scope.users.p._id = $scope.Auth._id;
				$scope.users.put(function(success){
					$('#ButtonModal').button('reset')
					if(success){
						Auth.getUser();
						$scope.Auth = Auth.user;
						LoadInfo();
						$('#CancelModal').show()
						$('#ButtonModal').button('reset')
						$('#DataUser').modal('hide')
					}else {
						$('#CancelModal').show()
						$('#ButtonModal').button('reset')
					}
					$('#alertEdit').addClass('hidden')
				})
			}else {
				$('#alertEdit').removeClass('hidden')
				$('#ButtonModal').button('reset')
				$('#CancelModal').show()
			}
		},
		EditSeudominio: function(){
			$('#CancelModalUsername').hide()
			$('#ButtonModalUsername').button('loading')
			let data = {};
			if(this.form.username != $scope.Auth.username)
				data.username = this.form.username;
			if(data.username){
				$('#ButtonModalUsername').button('loading')
				$scope.users.p = data;
				$scope.users.p._id = $scope.Auth._id;
				$scope.users.put(function(success){
					$('#ButtonModalUsername').button('reset')
					if(success){
						Auth.getUser();
						$scope.Auth = Auth.user;
						LoadInfo();
						$('#CancelModalUsername').show()
						$('#ButtonModalUsername').button('reset')
						$('#edit').modal('hide')
					}else {
						$('#CancelModalUsername').show()
						$('#ButtonModalUsername').button('reset')
					}
					$('#alertEditUsername').addClass('hidden')
				})
			}else {
				$('#alertEditUsername').removeClass('hidden')
				$('#ButtonModalUsername').button('reset')
				$('#CancelModalUsername').show()
			}
		},
		EditEmail: function(){
			$('#CancelModalEmail').hide()
			$('#ButtonModalEmail').button('loading')
			let data = {};
			if(this.form.mail != $scope.Auth.mail)
				data.mail = this.form.mail;
			if(data.mail){
				$('#ButtonModalEmail').button('loading')
				$scope.users.p = data;
				$scope.users.p._id = $scope.Auth._id;
				$scope.users.put(function(success){
					$('#ButtonModalEmail').button('reset')
					if(success){
						Auth.getUser();
						$scope.Auth = Auth.user;
						LoadInfo();
						$('#CancelModalEmail').show()
						$('#ButtonModalEmail').button('reset')
						$('#edit').modal('hide')
					}else {
						$('#CancelModalEmail').show()
						$('#ButtonModalEmail').button('reset')
					}
					$('#alertEditEmail').addClass('hidden')
				})
			}else {
				$('#alertEditEmail').removeClass('hidden')
				$('#ButtonModalEmail').button('reset')
				$('#CancelModalEmail').show()
			}
		},
		EditPassword: function(){
			$('#CancelModalPassword').hide()
			$('#ButtonModalPassword').button('loading')
			if(this.form.password || this.form.Oldpassword){
				$scope.users.p.password = this.form.password;
				$scope.users.p._id = $scope.Auth._id;
				$scope.users.put(function(success){
					$('#ButtonModalPassword').button('reset')
					if(success){
						$('#CancelModalPassword').show()
						$('#ButtonModalPassword').button('reset')
						$('#edit').modal('hide')
						Auth.setTokens(success.token,success.tokensecret)
							.then(function(){
								Auth.getUser();
								$scope.Auth = Auth.user;
								LoadInfo();
								$('#CancelModalPassword').show()
								$('#ButtonModalPassword').button('reset')
								$('#edit').modal('hide')
							})
					}else {
						$('#CancelModalPassword').show()
						$('#ButtonModalPassword').button('reset')
					}
					$('#alertEditPassword').addClass('hidden')
				})
			}else {
				$('#alertEditPassword').removeClass('hidden')
				$('#ButtonModalPassword').button('reset')
				$('#CancelModalPassword').show()
			}
		},
		EditStore: function(){

		},
		EditInfoBank: function(){
			let data = {};

			data.bank 	= this.form.bank;
			data.type 	= this.form.type;
			data.id 		= this.form.idp+'-'+this.form.document;
			data.name 	= this.form.nameComplete;
			data.number = this.form.number;
			data.mail 	= this.form.mail;
			data.user 	= $scope.Auth._id;

			$('#CancelModalInfoBanks').hide()
			$('#ButtonModalInfoBanks').button('loading')

			$scope.banksaccount.a = data;
			$scope.banksaccount.post(function(success){
				$('#ButtonModalInfoBanks').button('reset')
				if(success){

					$scope.send.form = {};
					LoadInfo();

					SweetAlert.swal({
						title: '¡Enhorabuena!',
						text: 'Haz registrado tu cuenta bancaria.',
						imageUrl: 'http://50.21.179.35:8083/bill-ok.png',
					  imageWidth: 150,
					  imageHeight: 150,
					  animation: false,
						confirmButtonText:"Continuar",
						showCancelButton: true
					},function(success){
						$('#CancelModalInfoBanks').show()
						$('#ButtonModalInfoBanks').button('reset')
						$('#edit').modal('hide')

						$scope.banksaccount.get()
					})
				}else {
					$('#CancelModalInfoBanks').show()
					$('#ButtonModalInfoBanks').button('reset')
				}
			})
		},
		DeleteBankAccount: function(id){
			SweetAlert.swal({
				text: '¿Estas seguro que deseas eliminar esta cuenta bancaria?',
				type: 'warning',
				confirmButtonText:"Continuar",
				showCancelButton: true
			},function(success){
				if(success){
					$scope.banksaccount.d._id = id;
					$scope.banksaccount.remove(function(success){
						if(success){
							SweetAlert.swal({
								type: 'success',
								text: "OK, se ha eliminado"
							},function(){
									$scope.banksaccount.get()
							});
						}
					})
				}
			})
		},
	}
	$scope.edit = {
		form: {},
		data: {},
		UpdateBankAccount: function(id){
		 	this.data = $scope.banksaccount.items.find(function(data){
				return data._id === id;
			})
			let document = this.data.id.split("-");
			this.form.bank 					= this.data.bank._id;
			this.form.type 					= this.data.type;
			this.form.number 				= this.data.number;
			this.form.idp 					= document[0];
			this.form.document			= document[1];
			this.form.nameComplete	= this.data.name;
			this.form.mail					= this.data.mail;
			this.form._id 					= this.data._id;
		},
		EditInfoBank: function(){
			let data = {},
			 		document = this.data.id.split("-");

			if(this.form.bank != this.data.bank._id)
				data.bank = this.form.bank;
			if(this.form.type != this.data.type)
				data.type = this.form.type;
			if(this.form.idp != document[0] || this.form.document != document[1])
				data.id = this.form.idp+'-'+this.form.document;
			if(this.form.nameComplete != this.data.name)
				data.name = this.form.nameComplete;
			if(this.form.number != this.data.number)
				data.number = this.form.number;
			if(this.form.mail != this.data.mail)
				data.mail = this.form.mail;

			data._id = this.form._id;

			$('#CancelModalEditBanks').hide()
			$('#ButtonModalEditBanks').button('loading')

			if(data.bank || data.type || data.id || data.name || data.number || data.mail){
				$scope.banksaccount.p = data;
				$scope.banksaccount.put(function(success){
					$('#ButtonModalEditBanks').button('reset')
					$('#CancelModalEditBanks').hide()
					if(success){
						$scope.banksaccount.get()
						$('#editBanks').modal('hide')
					}
				})
			}else {
				$('#alertEditBanks').show()
			}
		}
	}

	$('#DataUser').on('hidden.bs.modal', function () {
		$('#ButtonModal').button('reset')
		$('#CancelModal').show()
		$('#alertEdit').addClass('hidden')
	})

	$('#edit').on('hidden.bs.modal', function(){
		$('#ButtonModalUsername').button('reset')
		$('#CancelModalUsername').show()
		$('#alertEditUsername').addClass('hidden')

		$('#ButtonModalEmail').button('reset')
		$('#CancelModalEmail').show()
		$('#alertEditEmail').addClass('hidden')

		$('#ButtonModalPassword').button('reset')
		$('#CancelModalPassword').show()
		$('#alertEditPassword').addClass('hidden')

		$('#ButtonModalInfoBanks').button('reset')
		$('#CancelModalInfoBanks').show()
	})

	$('#editBanks').on('hidden.bs.modal', function(){
		$('#ButtonModalEditBanks').button('reset')
		$('#CancelModalEditBanks').show()
		$('#alertEditBanks').addClass('hidden')
	})

	function LoadInfo(){
		let document = $scope.Auth.document.split("-");
		$scope.send.form.nameComplete = $scope.Auth.name+' '+$scope.Auth.lastname;
		$scope.send.form.mail 			= $scope.Auth.mail;
		$scope.send.form.idp 				= document[0];
		$scope.send.form.document 	= document[1];
		$scope.send.form.name 			= $scope.Auth.name;
		$scope.send.form.lastname 	= $scope.Auth.lastname;
		$scope.send.form.username 	= $scope.Auth.username;
		$scope.send.form.phone 			= $scope.Auth.phone;
		$scope.send.form.address 		= $scope.Auth.address;
		$scope.send.form.localities = $scope.Auth.localities;
		$scope.send.form.bank 			= '';
		$scope.send.form.type 			= '';
		$scope.send.form.number 		= '';

		$scope.banksaccount.s.me = true;
		$scope.banksaccount.get()

	}


	if(!Auth.user){
		Auth.getUser();
	}else {
		$scope.Auth = Auth.user;
		LoadInfo();
	}
	if(Auth.user){
		$scope.Auth = Auth.user;
		LoadInfo();
	}else {
		$rootScope.$on('gotUser',function(){
			$scope.Auth = Auth.user;
			LoadInfo();
		})
	}
}

function SearchController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,SweetAlert,Transactions,Locality){
	if(Auth.user){
    $scope.auth = Auth.user;
  }else {
		$rootScope.$on('gotUser',function(){
      $scope.auth = Auth.user;
    })
  }

	$scope.users = angular.copy(Users);
	$scope.publications = angular.copy(Publications);
	$scope.predictiveSearch = angular.copy(Publications);
	$scope.category = angular.copy(Category);
	$scope.locality = angular.copy(Locality);
	$scope.transactions = angular.copy(Transactions);

	$scope.items = [];
	$scope.total = 1000;
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.limit = 8;
	$scope.limit2 = 4;
	$scope.limitt = $scope.limit+$scope.limit2;
	$scope.ban = false;

	$scope.publications.s = {
		start: $scope.limit*($scope.currentPage-1),
		limit: $scope.limit,
		status: 1
	}
	$scope.users.s = {
		start: $scope.limit*($scope.currentPage-1),
		limit: $scope.limit,
		status: 1
	}

	$scope.search = $stateParams.search;
	$scope.publications.s.name = $stateParams.search ? $stateParams.search: '';
	$scope.users.s.username = $stateParams.search ? $stateParams.search: '';
	$scope.category.s.name = $stateParams.search ? $stateParams.search: '';

	$scope.publications.s.localities = $stateParams.localities ? $stateParams.localities: '';
	$scope.publications.s.state = $stateParams.state ? $stateParams.state: '';
	$scope.publications.s.categories = $stateParams.category ? $stateParams.category: '';

	if($stateParams.orderBy==0)
		$scope.publications.s.orderDate = -1;
	if($stateParams.orderBy==1)
		$scope.publications.s.orderDate = 1;
	if($stateParams.orderBy==2)
		$scope.publications.s.orderSales = -1;
	if($stateParams.orderBy==3)
		$scope.publications.s.orderSales = 1;

	switch($state.current.options){
    case 'categories':
      $scope._Id = $stateParams.id;

      $scope.publications.s.categories = [$scope._Id];
      $scope.category.s._id = $scope._Id;

			$scope.predictiveSearch.s = {
				name: $scope.search,
				localities: $scope.publications.s.localities,
				categories: $scope.publications.s.categories,
				state: $scope.publications.s.state
			}
      $scope.predictiveSearch.getSearch(function(success){
				if(success)
					$scope.categories = $scope.predictiveSearch.itemSearch.categories;
					$scope.localities = $scope.predictiveSearch.itemSearch.localities;
					$scope.states 		= $scope.predictiveSearch.itemSearch.states;
			})
      $scope.category.getSingle(function(success){ })
      $scope.publications.get(function(success) {
        if(success)
          $scope.items = $scope.publications.items;
					$scope.total = $scope.publications.total;
					if($scope.total>0)
						$scope.ban = true;
      })
    break;
    case 'search':
      $scope.search = $stateParams.search;

			if($stateParams.category){
				$scope.category.s._id = $stateParams.category;
				$scope.category.get(function(success){})
			}
			if($stateParams.localities){
				$scope.locality.s._id = $stateParams.localities;
				$scope.locality.get(function(success){})
			}

      $scope.publications.get(function(success) {
          if(success){
						if($scope.publications.total>0){
							$scope.items = $scope.publications.items;
						}
						$scope.users.get(function(success){
							if(success){
								if($scope.users.total>0 && $scope.publications.total===0){
									$scope.items = $scope.users.items;
									if($scope.publications.total===0){
										$scope.total = $scope.users.total;
									}else {
										$scope.total += $scope.users.total;
									}
								}
								if($scope.users.total>0 && $scope.publications.total>0){
									$scope.limit2 = 4;
									$scope.items.splice(2,0,$scope.users.items[0])
									if($scope.users.total>1){
										$scope.items.splice(3,0,$scope.users.items[1])
									}
									$scope.total = $scope.publications.total;
									$scope.total += $scope.users.total;
									console.log($scope.total)
									if($scope.total>0)
										$scope.ban = true;
								}
								if($scope.publications.total>0 && $scope.users.total===0){
									$scope.total = $scope.publications.total;
									$scope.total += $scope.users.total;
									console.log($scope.total)
									if($scope.total>0)
										$scope.ban = true;
								}
							}
						})
					}
      })
			$scope.predictiveSearch.s = {
				name: $scope.search,
				localities: $scope.publications.s.localities,
				categories: $scope.publications.s.categories,
				state: $scope.publications.s.state
			}
      $scope.predictiveSearch.getSearch(function(success){
				if(success)
					$scope.categories = $scope.predictiveSearch.itemSearch.categories;
					$scope.localities = $scope.predictiveSearch.itemSearch.localities;
					$scope.states 		= $scope.predictiveSearch.itemSearch.states;
			})
    break;
		case 'list':
			if($stateParams.category){
				$scope.category.s._id = $stateParams.category;
				$scope.category.get(function(success){})
			}
			if($stateParams.localities){
				$scope.locality.s._id = $stateParams.localities;
				$scope.locality.get(function(success){})
			}
			$scope.predictiveSearch.s = {
				name: $scope.search,
				localities: $scope.publications.s.localities,
				categories: $scope.publications.s.categories,
				state: $scope.publications.s.state
			}
      $scope.predictiveSearch.getSearch(function(success){
				if(success)
					$scope.categories = $scope.predictiveSearch.itemSearch.categories;
					$scope.localities = $scope.predictiveSearch.itemSearch.localities;
					$scope.states 		= $scope.predictiveSearch.itemSearch.states;
			})

			$scope.publications.get(function(success) {
				$scope.items = $scope.publications.items;
				$scope.total = $scope.publications.total;
				if($scope.total>0)
					$scope.ban = true;
      })
		break;
  }

	$scope.setPage = function (pageNo){
    $scope.currentPage = pageNo;
		console.log(pageNo)
  };
	$scope.pageChanged = function(){
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.setCategory = function(id){
		$state.go('.',{category: id})
	}
	$scope.setLocalities = function(id){
		$state.go('.',{localities: id})
	}
	$scope.setState = function(state){
		$state.go('.',{state: state})
	}
	$scope.setOrderBy = function(){
		$state.go('.',{orderBy: $scope.OrderSort})
	}

	$scope.purchase = {};
	$scope.purchase = function($index){
		$scope.purchase.item = $scope.items[$index];
		$scope.purchaseID = $scope.items[$index]._id;
	}
	$scope.quantity = 1;
	$scope.quantityF = function(params){
		if(params){
			if($scope.quantity < $scope.purchase.item.stock)
				$scope.quantity+= 1;
		}else {
			if($scope.quantity > 1){
				$scope.quantity-= 1;
			}
		}
	}
	$scope.purchaseConfirm = function(){
		SweetAlert.swal({
			title: '',
			text: '¿Estas seguro de tu compra?',
			type: 'info',
			confirmButtonText:"Continuar",
			showCancelButton: true,
		},function(success){
			if(success.value){
				$('#purchase').modal('hide');
				$timeout(function(){
					$('#purchaseSuccess').modal('show');
				},500)
			}
		})
	}
	$scope.purchaseSend = function(flag){
		if(flag){
			$scope.transactions.a.type = 0;
		}else {
			$scope.transactions.a.type = 1;
		}
		$scope.transactions.a._id = $scope.purchaseID;
		$scope.transactions.a.quantity = $scope.quantity;

		$scope.transactions.post(function(success,id = null){
			if(success){
				$('#purchaseSuccess').modal('hide');
				$timeout(function(){
					$state.go('base.checkout',{id: id});
				},500)
			}
		})
	}

	$scope.category.s = {
    parent: "false"
  }
  $scope.category.get(function(success){
    if(success){
      $scope.ParentCat = $scope.category.items;
      $scope.categorySingle($scope.ParentCat[0]._id);
    }
  })
  $scope.categorySingle = function(Id){
    if(Id){
      $scope.category.s = {
          parent: Id
      }
      GetCategory();

    }else {
      $scope.category.s = {
        parent: Id
      }
      GetCategory();
    }
    function GetCategory(){
      $scope.category.get(function(success){
        if(success){
          $scope.listCat = $scope.category.items;
          $scope.active = $scope.category.s.parent;
        }
      })
    }
  }

}

function PublicationController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Comments,SweetAlert,Transactions){
  if(Auth.isLogged())
    $scope.follow = true;
	if(Auth.user){
    $scope.auth = Auth.user;
  }else {
		$rootScope.$on('gotUser',function(){
      $scope.auth = Auth.user;
    })
  }
  $scope.id = $stateParams.id;

  $scope.publications = angular.copy(Publications);
  $scope.comments = angular.copy(Comments);
	$scope.transactions = angular.copy(Transactions);

  $scope.images = [];
  $scope.index = null;

  $scope.publications.getSingle($scope.id,function(success){
    if(success){
      $rootScope.title = $scope.publications.item.user.username+' - '+$scope.publications.item.name;
      $scope.average = ($scope.publications.item.user.rating.seller.total/$scope.publications.item.user.rating.seller.totalTallied);
      if(isNaN($scope.average))
        $scope.average = 0;
      $scope.comments.s.publication = $scope.publications.item._id;
      $scope.comments.get(function(success){})
    }
  })

  $scope.send = {
    form: {},
    comments: function(){
      $scope.comments.a = this.form;
      $scope.comments.a.publication = $scope.publications.item._id;
      $scope.comments.post(function(success){
        if(success)
          $scope.send.form = {};
		      $scope.comments.get(function(success){})
      })
    }
  }


  let letter = 0;
  $scope.letter = {
    count: 240,
    on: function(){
      if($scope.send.form.comment.length>letter)
        this.count = this.count - 1;
      else
        this.count = this.count + 1;

      letter = $scope.send.form.comment.length;
    }
  }

	$scope.quantity = 1;
	$scope.quantityF = function(params){
		if(params){
			if($scope.quantity < $scope.publications.item.stock)
				$scope.quantity+= 1;
		}else {
			if($scope.quantity > 1){
				$scope.quantity-= 1;
			}
		}
	}
	$scope.purchaseConfirm = function(){
		SweetAlert.swal({
			title: '',
			text: '¿Estas seguro de tu compra?',
			type: 'info',
			confirmButtonText:"Continuar",
			showCancelButton: true,
		},function(success){
			if(success.value){
				$('#purchase').modal('hide');
				$timeout(function(){
					$('#purchaseSuccess').modal('show');
				},500)
			}
		})
	}

	$scope.purchase = function(flag){
		if(flag){
			$scope.transactions.a.type = 0;
		}else {
			$scope.transactions.a.type = 1;
		}
		$scope.transactions.a._id = $scope.publications.item._id;
		$scope.transactions.a.quantity = $scope.quantity;

		$scope.transactions.post(function(success,id = null){
			if(success){
				$('#purchaseSuccess').modal('hide');
				$timeout(function(){
					$state.go('base.checkout',{id: id});
				},500)
			}
		})
	}
}

function UserController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Follows,Friendships) {
  if(Auth.isLogged())
    $scope.follow = true;
  $rootScope.id = $stateParams.id;
  $scope.self = false;
  $scope._id = $rootScope.id;

  $scope.users = angular.copy(Users);
  $scope.users.getSingle($rootScope.id,function(success){
		if(success){
			$scope.info = $scope.users.item;
			console.log($scope.info);
		}
  })

	$scope.follows = angular.copy(Follows);
	$scope.follows.s = {
		fromMe: true,
		follows: true,
		_id: $scope._id,
		limit: 1,
	}
	$scope.followers = angular.copy(Follows);
	$scope.followers.s = {
		toMe: true,
		followers: true,
		_id: $scope._id,
		limit: 1
	}
	$scope.friendships = angular.copy(Friendships);
	$scope.friendships.s = {
		_id: $scope._id,
		limit: 1
	}
	$scope.change = function(){
		$scope.follows.get(function(success){})
		$scope.followers.get(function(success){})
		$scope.friendships.get(function(success){})
	}
	$scope.change()
}

function ProfileController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Upload,Follows,SweetAlert,Friendships) {
  if(!Auth.isLogged())
    return $state.go('base.index');

	$scope.users = angular.copy(Users);

	$rootScope.id = false;
  $scope.self = true;

  if(Auth.user){
    $scope.info = Auth.user;
		$scope._id = $scope.info._id;
		$rootScope.title = Auth.user.username;
  }else {
    $rootScope.$on('gotUser',function(){
      $scope.info = Auth.user;
			$scope._id = $scope.info._id;
			$rootScope.title = Auth.user.username;
    })
  }

	$scope.outputImageCover = null;
	$scope.inputImageCover = null;
	$scope.outputImagePhoto = null;
	$scope.inputImagePhoto = null;

	$scope.onUpdate = function(data) {};

	$scope.send = {
		form: {},
		validationCover: function(file,width,height){
			if(file)
			if(width<940 && height<300)
			return SweetAlert.swal({
				title:'Ohh, A ocurrido un error',
				text: 'Debes seleccionar una imagen con un minimo de ancho de 940px y alto de 300px',
				type: 'error'
			},function(success){})
			else
			this.form.cover = file;
			this.ModalEdit();
		},
		validationPhoto: function(file,width,height){
			$('#EditImage').button('reset');
			$('#EditImageReset').show();
			if(file)
				if(width<160 && height<160)
					return SweetAlert.swal({
						title:'Ohh, A ocurrido un error',
						text: 'Debes seleccionar una imagen con un minimo de ancho y alto de 160px',
						type: 'error'
					},function(success){})
				else
					this.form.photo = file;
					this.ModalEdit();
		},
		ModalEdit: function(){
			if(this.form.cover)
				$('#Preview').modal('show');
				Upload.dataUrl(this.form.cover,false)
				.then(function(urls){
					$scope.inputImageCover = urls;
				});
			if(this.form.photo)
				$('#Preview').modal('show');
				Upload.dataUrl(this.form.photo,false)
				.then(function(urls){
					$scope.inputImagePhoto = urls;
				});
		},
		send: function(){
			$('#EditImage').button('loading')
			$('#EditImageReset').hide()
			if($scope.inputImageCover)
				$scope.users.p = {
					_id: Auth.user._id,
					cover64: $scope.outputImageCover
				}
			if($scope.inputImagePhoto)
				$scope.users.p = {
					_id: Auth.user._id,
					photo64: $scope.outputImagePhoto
				}
			$scope.users.putSelf($scope.users.p,function(success){
				if(success)
					Auth.getUser();
					$('#EditImage').button('reset');
					$('#EditImageReset').show();
					$('#Preview').modal('hide');
			})
		}
	}
	$('#Preview').on('hidden.bs.modal', function () {
		$scope.send.form = {};

	  $scope.outputImageCover = null;
	  $scope.inputImageCover = null;

	  $scope.outputImagePhoto = null;
	  $scope.inputImagePhoto = null;
	})
	$scope.follows = angular.copy(Follows);
	$scope.follows.s = {
		fromMe: true,
		follows: true,
		limit: 1,
	}
	$scope.followers = angular.copy(Follows);
	$scope.followers.s = {
		toMe: true,
		followers: true,
		limit: 1
	}
	$scope.friendships = angular.copy(Friendships);
	$scope.friendships.s = {
		toMe: true,
		limit: 1
	}
	$scope.change = function(){
		$scope.follows.getSelf(function(success){})
		$scope.followers.getSelf(function(success){})
		$scope.friendships.getSelf(function(success){})
	}
	$scope.change()
}

function ProfilePublicationController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views) {
	$scope.publications = angular.copy(Publications);

	$scope.items = [];
	$scope.total = 1000;
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.limit = 10;

	$scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
		console.log(pageNo)
  };

	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.orderColum = {
    list: function(){},
    grid: function(){}
  }
	if(!$scope.currentPage>1){
		$scope.publications.s = {
			start: 0,
			limit: $scope.limit
		}
	}else {
		$scope.publications.s = {
			start: $scope.limit*($scope.currentPage-1),
			limit: $scope.limit
		}
	}

  if($rootScope.id){
		$scope.publications.s.user = $stateParams.id;
	}else {
		$scope.publications.s.byMe = true;
	}

	$scope.publications.get(function(success){
		if(success){
			$scope.items = $scope.publications.items;
			$scope.total = $scope.publications.total;
		}
		// if(success)
		//   $scope.publications.s = {};
		// else
		//   $scope.publications.s = {};
	})

  $scope.send = {
    form: {},
    search: function(){
			$scope.publications.s.name = this.form.search;
			$scope.publications.get(function(success){
				if(success){
					$scope.items = $scope.publications.items;
					$scope.total = $scope.publications.total;
				}
			})
			console.log(this.form)
		}
  }
}

function ProfileInfoController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views) {
  $scope.users = angular.copy(Users)

  if(!$rootScope.id){
    $scope.actionOn = true;
    $scope.action = {
      form: {},
			ModalEdit: function(options){
				$scope.options = options;
				$('#ModalEdit').modal('show');
			},
      edit: function(){
				$('#CancelModal').hide()
				$('#ButtonModal').button('loading')
				let form = {};

				if($scope.data.mail != this.form.mail)
					form.mail = this.form.mail;
				if($scope.data.phone != this.form.phone)
					form.phone = this.form.phone;
				if($scope.data.website != this.form.website)
					form.website = this.form.website;
				if($scope.data.address != this.form.address)
					form.address = this.form.address;

				if(form.mail || form.phone || form.website || form.address){
					$('#ButtonModal').button('loading')
					$scope.users.putSelf(form,function(success){
						$('#ButtonModal').button('reset')
						if(success){
							this.modalForm= {};
							Auth.getUser(loadMe);
							$('#CancelModal').show()
							$('#ButtonModal').button('reset')
							$('#ModalEdit').modal('hide')
						}else {
							$('#CancelModal').show()
							$('#ButtonModal').button('reset')
						}
						$('#alertEdit').addClass('hidden')
					})
				}else {
					$('#alertEdit').removeClass('hidden')
					$('#ButtonModal').button('reset')
					$('#CancelModal').show()
				}
      }
    }
		$('#ModalEdit').on('hidden.bs.modal', function () {
			$('#ButtonModal').button('reset')
			$('#CancelModal').show()
			$('#alertEdit').addClass('hidden')
		})

    if(Auth.user){
      loadMe();
    }
    else {
      $rootScope.$on('gotUser',function(){
        loadMe();
      })
    }
  }else {
    $scope.actionOn = false;
    $scope.users.getSingle($stateParams.id,function(success){
      loadMe();
    })
  }

  function loadMe(){
    if(!$rootScope.id){
      $scope.average = (Auth.user.rating.seller.total/Auth.user.rating.seller.totalTallied);
      if(Number.isNaN($scope.average))
        $scope.average = 0;

      if(Auth.user.website){
        $scope.action.form.website = angular.copy(Auth.user.website);
      }else {
        $scope.action.form.website = 'No posee sitio web';
      }
      $scope.action.form.phone        = angular.copy(Auth.user.phone);
      $scope.action.form.mail         = angular.copy(Auth.user.mail);
      $scope.action.form.address      = angular.copy(Auth.user.address);
      $scope.action.form.description  = angular.copy(Auth.user.description);
      $scope.action.form._id          = angular.copy(Auth.user._id);
      $scope.action.form.nomb         = angular.copy(Auth.user.name)+' '+angular.copy(Auth.user.lastname);
			$scope.data = Auth.user;
    }else {
      $scope.action = {
        form: {}
      };
      $scope.average = ($scope.users.item.rating.seller.total/$scope.users.item.rating.seller.totalTallied);
      if(Number.isNaN($scope.average))
        $scope.average = 0;

      if($scope.users.item.website){
        $scope.action.form.website = angular.copy($scope.users.item.website);
      }else {
        $scope.action.form.website = 'No posee sitio web';
      }
      $scope.action.form.phone        = angular.copy($scope.users.item.phone);
      $scope.action.form.mail         = angular.copy($scope.users.item.mail);
      $scope.action.form.address      = angular.copy($scope.users.item.address);
      $scope.action.form.description  = angular.copy($scope.users.item.description);
      $scope.action.form._id          = angular.copy($scope.users.item._id);
      $scope.action.form.nomb         = angular.copy($scope.users.item.name)+' '+angular.copy($scope.users.item.lastname);
    }
  }
}

function ProfileFriendController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Friendships) {
  $scope.friendships = angular.copy(Friendships);

	$scope.items = [];
	$scope.total = 1000;
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.limit = 10;
	$scope.flag = false;

	$scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
		console.log(pageNo)
  };

	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.friendships.s = {
		start: $scope.limit*($scope.currentPage-1),
		limit: $scope.limit
	}

	$scope.friendships.s.search = $stateParams.search ? $stateParams.search: '';
	$scope.friendships.s.orderBy = $stateParams.orderBy ? $stateParams.orderBy: '';

  $scope.orderColum = {
    list: function(){},
    grid: function(){}
  }

	$scope.change = function(){
		if($scope.$parent.self){
			$scope.friendships.s.toMe = true;
			$scope.friendships.getSelf(function(success){
				if(success){
					$scope.items = $scope.friendships.items;
					$scope.total = $scope.friendships.total;
				}
			})
		} else{
			$scope.friendships.s._id = $rootScope.id;
			$scope.friendships.get(function(success){
				if(success){
					$scope.items = $scope.friendships.items;
					$scope.total = $scope.friendships.total;
				}
			})
		}
	}
  $scope.send = {
    form: {},
		search: function(){
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(){
			$state.go('.',{orderBy: this.form.filter});
		}
  }

	if($stateParams.search)
		$scope.send.form.search = $stateParams.search;

	$scope.change()
}

function ProfileFollowingController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Follows) {
  $scope.follows = angular.copy(Follows);

	$scope.items = [];
	$scope.total = 1000;
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.limit = 10;
	$scope.flag = false;

	$scope.follows.s = {
		start: $scope.limit*($scope.currentPage-1),
		limit: $scope.limit,
		fromMe: true,
		follows: true
	}

	$scope.follows.s.search = $stateParams.search ? $stateParams.search: '';
	$scope.follows.s.orderBy = $stateParams.orderBy ? $stateParams.orderBy: '';

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};

	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

  $scope.orderColum = {
    list: function(){},
    grid: function(){}
  }
	$scope.change = function(){
		if($scope.$parent.self)
			$scope.follows.getSelf(function(success){
				if(success){
					$scope.items = $scope.follows.items;
					$scope.total = $scope.follows.total;
					if($scope.total>0)
						$scope.flag = true;
				}
			})
		else{
			$scope.follows.s._id = $scope.$parent.id;
			$scope.follows.get(function(success){
				if(success){
					$scope.items = $scope.follows.items;
					$scope.total = $scope.follows.total;
					if($scope.total>0)
						$scope.flag = true;
				}
			})
		}
	}

  $scope.send = {
    form: {},
    search: function(){
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(){
			$state.go('.',{orderBy: this.form.filter});
		}
  }
	if($stateParams.search)
		$scope.send.form.search = $stateParams.search;

	$scope.change()

	console.log($scope.$parent)
}

function ReputationController(Auth, $scope, $rootScope, $state){
	if(!Auth.isLogged())
		return $state.go('base.index');
	$rootScope.title = $state.current.title;
  if(Auth.user){
    loadMe();
  }
  else {
    $rootScope.$on('gotUser',function(){
      loadMe();
    })
  }
  $scope.Auth = Auth;

  function loadMe(){

		$scope.average = (Auth.user.rating.seller.totalTallied/Auth.user.rating.seller.total*10);
    if(isNaN($scope.average))
      $scope.average = 0;
  }
}

function ProfileFollowerController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Follows) {
	$scope.follows = angular.copy(Follows);

	$scope.items = [];
	$scope.total = 1000;
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.limit = 12;
	$scope.flag = false;

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};

	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.follows.s = {
		start: $scope.limit*($scope.currentPage-1),
		limit: $scope.limit,
		toMe: true,
		followers: true
	}

	$scope.follows.s.search = $stateParams.search ? $stateParams.search: '';
	$scope.follows.s.orderBy = $stateParams.orderBy ? $stateParams.orderBy: '';

	$scope.orderColum = {
		list: function(){},
		grid: function(){}
	}

	$scope.change = function(){
		if($scope.$parent.self){
			$scope.follows.getSelf(function(success){
				if(success){
					$scope.items = $scope.follows.items;
					$scope.total = $scope.follows.total;
					if($scope.total>0)
						$scope.flag = true;
				}
			})

		}else {
			$scope.follows.s._id = $scope.$parent.id;
			$scope.follows.get(function(success){
				if(success){
					$scope.items = $scope.follows.items;
					$scope.total = $scope.follows.total;
					if($scope.total>0)
						$scope.flag = true;
				}
			})
		}
	}
	$scope.send = {
		form: {},
		search: function(){
			console.log(this.form.search)
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(){
			$state.go('.',{orderBy: this.form.filter});
		}
	}

	if($stateParams.search)
		$scope.send.form.search = $stateParams.search;

	$scope.change()
}

function PublishController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth){
	$rootScope.title = $state.current.title;
}

function PublishBaseController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Category){
	$rootScope.title = $state.current.title;

	$rootScope.activeOne = true;
	$rootScope.activeTwo = false;
	$rootScope.activeThird = false;

	$scope.category = angular.copy(Category);
	$scope.category.s = {
		parent: "false"
	}
	$scope.category.get();

	$rootScope.disabledOne   = false;
	$rootScope.disabledTwo   = true;
	$rootScope.disabledThird = true;

	// $rootScope.redirection = function(){
	// 	$state.go('publish.base');
	// }
}

function PublishOneController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Category){
	$rootScope.title = $state.current.title;
	$scope.categories = [];
	$scope.data = [];

	$rootScope.activeOne = true;
	$rootScope.activeTwo = false;
	$rootScope.activeThird = false;

	$scope.category = angular.copy(Category);
	$scope.category.s._id = $stateParams.selector[0];
	$scope.category.getSingle(function(success){
		if(success)
			if($stateParams.selector.length == 1){
				$scope.data.push($scope.category.item._id)
				$scope.category.s = {
					parent: $stateParams.selector[0]
				}
				$scope.category.get(function(success){
					if(success){
						if($scope.category.items.length){
							$scope.categories.push($scope.category.items)
						}
						else{
							$scope.next = true;
						}
					}
				});
			}
	});

	$scope.active = [];
	$scope.next = false;

	delete $rootScope.data;

	$scope.GetCategory = function(Id,index){
		$scope.active[index] = Id;
		$scope.data[index+1] = Id;


		$scope.category.s = {
			parent: Id
		}
		$scope.category.get(function(success){
			if(success)
				if($scope.category.items.length){
					$scope.categories[index+1] = $scope.category.items;
					$scope.next = false;
				}else{
					$scope.categories.splice(index+1,1);
					$scope.data.splice(index+2,1);
					$scope.next = true;
				}
		});
	}

	function GetCategory(Id,count){
		$scope.active[count] = Id;
		$scope.category.s = {
			parent: Id
		}
		$scope.category.get(function(success){
			if(success)
				if($scope.category.items.length){
					$scope.categories[count] = $scope.category.items;
					$scope.next = false;
				}else{
					$scope.next = true;
				}
				$scope.data[count] = Id;
		});
	}

	if($stateParams.selector.length > 1){
		for(i=0;i<$stateParams.selector.length;i++){
			GetCategory($stateParams.selector[i],i)
		}
	}

	$rootScope.disabledOne   = false;
	$rootScope.disabledTwo   = true;
	$rootScope.disabledThird = true;

	$rootScope.redirection = function(){
		$state.go('publish.base');
	}
}

function PublishTwoController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Category,textAngularManager,Upload,Locality,SweetAlert){
	$rootScope.title = $state.current.title;

	$rootScope.activeOne = false;
	$rootScope.activeTwo = true;
	$rootScope.activeThird = false;
	$scope.flag = false;

	$scope.category = angular.copy(Category);

	$scope.selector = $stateParams.selector;

	if(!$stateParams.selector)
		return $state.go('publish.base');

	$scope.photos = [];
	$scope.inputPhotos = [];
	$scope.outPhotos = [];
	$scope.i = 0;

	$scope.categories = [];
	$scope.disabled = [];

	function GetCategories(i){
		$scope.category.s._id = $scope.selector[i];
		$scope.category.getSingle(function(success){
			if(success)
				$scope.categories[i] = $scope.category.item;
		})
	}
	for(i=0;i<$scope.selector.length;i++)
		GetCategories(i);

	// Upload.dataUrl(this.photos[i],false)
	// .then(function(urls){
	// 	photos[i] = urls;
	// });

	$scope.PhotoEdit = function(index){
		$scope.i = index;
		$('#Preview').modal('show');
		Upload.dataUrl($scope.photos[index],false)
		.then(function(urls){
			if(urls)
				$scope.inputPhotos[index] = urls;
				// $scope.disabled[index] = true;
		});
	}
	$scope.disabled = function(index){
		$scope.disabled[index] = false;
		$scope.photos[index] = false;
		$scope.outPhotos[index] = false;
	}
	$scope.on = {
		form: {},
		submit: function(){
			$('#Continue').button('loading')
			this.form.files64 = [];
			this.form.localities = $scope.onLocality.out;
			for(var x in $scope.outPhotos)
				if($scope.outPhotos[x]!=undefined)
		 			this.form.files64.push($scope.outPhotos[x]);

			if(this.form.files64.length<1){
				return SweetAlert.swal({
					title:'No has incluido imagenes de tu producto',
					text: 'Debes incluir al menos un imagen de tu producto para poder ser publicado',
					type: 'error'
				},function(success){})
			}else {
				$rootScope.data = this.form;
				$('#Continue').button('reset')
				$state.go('publish.third',{selector: $scope.selector})
			}

		}
	}

	$scope.onLocality = {
		action: angular.copy(Locality),
		collections: [],
		out: [],
		parent: function(i){
			this.action.getChilds(this.out[i-1],function(success,items){
				if(success)
					this.collections[i] = items;
			}.bind(this))
		},
		init: function(){
			this.action.s.parent = false;
			this.action.get(function(success){
				if(success)
					this.collections[0] = this.action.items
			}.bind(this),false)
		}
	}
	$scope.onLocality.init();

	if($rootScope.data){
		$scope.$watch('outPhotos[0]',function(){
			$scope.photos 			= angular.copy($rootScope.data.files64);
			$scope.inputPhotos 	= angular.copy($rootScope.data.files64);
			$scope.outPhotos 		= angular.copy($rootScope.data.files64);
			$scope.on.form 			= angular.copy($rootScope.data);
		})
	}

	$rootScope.disabledOne   = false;
	$rootScope.disabledTwo   = false;
	$rootScope.disabledThird = true;
	$rootScope.redirection = function(id){
		console.log(id)
		if(id==undefined){
			$state.go('publish.one',{selector: $scope.selector});
		}else{
			// $state.go('publish.two',{selector: $scope.selector});
		}
	}
}

function PublishThirdController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,SweetAlert){
	$rootScope.title = $state.current.title;

	$rootScope.redirection = function(id){
		if(id==undefined){
			$state.go('publish.one',{selector: $scope.on.form.categories});
		}else if(id==1){
			$state.go('publish.two',{selector: $stateParams.selector});
		}
	}

	if(!$stateParams.selector && $rootScope.data==undefined){
		console.log('!params');
		return $state.go('publish.base');
	}else if($stateParams.selector && $rootScope.data==undefined) {
		return $rootScope.redirection(1);
	}

	$rootScope.activeOne = false;
	$rootScope.activeTwo = false;
	$rootScope.activeThird = true;

	console.log($rootScope.data)

	$scope.publication = angular.copy(Publications);

	$scope.on = {
		form: {},
		submit: function(){
			this.form.categories = $stateParams.selector;
			$scope.publication.a = this.form;
			let startdate = this.form.startdate;
			$scope.publication.post(function(success,id){
				if(success)
					SweetAlert.swal({
						title: 'Exito',
						text: 'Tu publicacion estara disponible en nuestros listados el'+startdate,
						imageUrl: 'http://50.21.179.35:8083/bill-ok.png',
						imageWidth: 150,
						imageHeight: 150,
						animation: false,
						confirmButtonText:"Continuar",
						showCancelButton: true,
					},function(success){
						if(success){
							$state.go('base.publication',{id: id});
						}
					});
					delete $rootScope.data;
			})
		}
	}

	$scope.on.form = $rootScope.data;

	$rootScope.disabledOne   = false;
	$rootScope.disabledTwo   = false;
	$rootScope.disabledThird = false;
}

function checkoutController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,BanksAccount,Transactions){
	$rootScope.title = $state.current.title;

	$scope.id = $stateParams.id;

	$scope.transactions = angular.copy(Transactions);
	$scope.banksaccount = angular.copy(BanksAccount);

	$scope.transactions.s._id = $scope.id;
	$scope.transactions.getSingle(function(){})

	$scope.banksaccount.s.me = true;
	$scope.banksaccount.get(function(success){})
}

function bankcheckout(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,BanksAccount){
	$rootScope.title = $state.current.title;
}

function bankcheckoutOne(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Transactions,BanksAccount,PanaPagos){
	$rootScope.title = $state.current.title;

	$scope.transactions = angular.copy(Transactions);
	$scope.panaPago = angular.copy(PanaPagos);

	$rootScope.disabledOne = false;
	$rootScope.disabledTwo = true;
	$rootScope.disabledThird = true;

	$rootScope.activeOne = true;
	$rootScope.activeTwo = false;
	$rootScope.activeThird = false;

	$scope.on = {
		form: {},
		submit: function(){
			$('#Report').button('loading')

			if($scope.type === 0){
				$scope.transactions.p = this.form;
				$scope.transactions.p._id = $stateParams.id;
				$scope.transactions.put(0,function(success){
					$('#Report').button('reset')
					if(success){
						$rootScope.dataOne = this.form;
						$rootScope.redirection(1);
					}
				})
			}
			if($scope.type === 1){
				console.log(this.form)
				$scope.panaPago.a = this.form;
				$scope.panaPago.a.way = this.form.type;
				$scope.panaPago.a._id = $stateParams.id;
				delete $scope.panaPago.a.type;
				$scope.panaPago.post(function(success){
					$('#Report').button('reset')
					if(success){
						$rootScope.dataOne = this.form;
						$rootScope.redirection(1);
					}
				})
			}
		}
	}

	// type === 0 pagar al vendedor

	$scope.transactions.s._id = $stateParams.id;
	$scope.transactions.getSingle(function(success){
		if(success){
			$scope.on.form.sum = $scope.transactions.item.price;
			$scope.type = $scope.transactions.item.type;
		}
	})

	if($rootScope.dataOne)
		$scope.on.form = $rootScope.dataOne;

	$scope.banksaccount = [
			{
				name: 'Venezuela',
				value: 1
			},
			{
				name: 'Banesco',
				value: 2
			},
			{
				name: 'Provincial',
				value: 3
			},
			{
				name: 'Mercantil',
				value: 4
			},
			{
				name: 'BOD',
				value: 5
			},
			{
				name: 'Bicentenario',
				value: 6
			},
			{
				name: 'Del Tesoro',
				value: 7
			},
			{
				name: 'Bancaribe',
				value: 8
			},
			{
				name: 'Exterior',
				value: 9
			},
			{
				name: 'BNC',
				valule: 10
			},
			{
				name: 'BFC',
				value: 11
			},
			{
				name: 'BVC',
				value: 12
			},
			{
				name: 'Industrial',
				value: 13
			},
			{
				name: 'Caroní',
				value: 14
			},
			{
				name: 'Sofitasa',
				value: 15
			},
			{
				name: 'Banplus',
				value: 16
			},
			{
				name: 'Plaza',
				value: 17
			},
			{
				name: 'Activo',
				value: 18
			},
			{
				name: 'Bancrecer',
				value: 19
			},
			{
				name: 'Del Sur',
				value: 20
			},
			{
				name: '100% Banco',
				value: 21
			},
			{
				name: 'Agricola',
				value: 22
			},
			{
				name: 'Citibank',
				value: 23
			},
			{
				name: 'BANFANB',
				value: 24
			},
			{
				name: 'Bancamiga',
				value: 25
			},
			{
				name: 'Bangente',
				value: 26
			},
			{
				name: 'Mi Banco',
				value: 27
			},
			{
				name: 'Novo Banco',
				value: 28
			}
		]

	$rootScope.redirection = function(i) {
		switch(i){
			case undefined:
				break;
			case 1:
				$state.go('bankcheckout.bankcheckoutTwo',{id: $stateParams.id});
				break;
			case 2:
				$state.go('bankcheckout.bankcheckoutThird',{id: $stateParams.id});
				break;
		}
	}

}

function bankcheckoutTwo(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Transactions){
	$rootScope.title = $state.current.title;

	$scope.transactions = angular.copy(Transactions);

	$scope.id = $stateParams.id;

	$rootScope.disabledOne = false;
	$rootScope.disabledTwo = false;
	$rootScope.disabledThird = true;

	$rootScope.activeOne = false;
	$rootScope.activeTwo = true;
	$rootScope.activeThird = false;

	$scope.disabled = true;

	$scope.on = {
		form: {},
		submit: function(){
			$scope.transactions.p = this.form;
			$scope.transactions.p._id = $stateParams.id;
			$('#Report').button('loading')
			$scope.transactions.put(1,function(success){
				$('#Report').button('reset')
				if(success){
					$rootScope.redirection(2);
				}
			})
		}
	}

	$scope.transactions.p.idp = 'v';

	$scope.change = function(){
		$scope.disabled = false;
	}

	$scope.transactions.s._id = $stateParams.id;
	$scope.transactions.getSingle(function(success){
		if(success){
			let document = $scope.transactions.item.buyer.document.split('-')
			$scope.on.form.name = $scope.transactions.item.buyer.name+' '+$scope.transactions.item.buyer.lastname;
			$scope.on.form.id = document[1];
			$scope.on.form.idp = document[0];
			if($scope.transactions.item.buyer.localities.length>1)
				for(var i in $scope.transactions.item.buyer.localities)
					if($scope.on.form.address==undefined)
						$scope.on.form.address = $scope.transactions.item.buyer.localities[i].name;
					else
						$scope.on.form.address += ', '+$scope.transactions.item.buyer.localities[i].name;
		}
	})

	$rootScope.redirection = function(i) {
		switch(i){
			case undefined:
				$state.go('bankcheckout.bankcheckoutOne',{id: $stateParams.id});
				break;
			case 1:
				$state.go('bankcheckout.bankcheckoutTwo',{id: $stateParams.id});
				break;
			case 2:
				$state.go('bankcheckout.bankcheckoutThird',{id: $stateParams.id});
				break;
		}
	}

}

function bankcheckoutThird(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Transactions,SweetAlert){
	$rootScope.title = $state.current.title;

	$scope.transactions = angular.copy(Transactions);

	$rootScope.disabledOne = false;
	$rootScope.disabledTwo = false;
	$rootScope.disabledThird = false;

	$rootScope.activeOne = false;
	$rootScope.activeTwo = false;
	$rootScope.activeThird = true;

	$scope.on = {
		form: {},
		submit: function(){
			console.log(this.form);
			$scope.transactions.p = this.form;
			$scope.transactions.p._id = $stateParams.id;
			$('#Report').button('loading')
			$scope.transactions.put(2,function(success){
				$('#Report').button('reset')
				if(success){
					SweetAlert.swal({
					  title: '',
					  text: 'Tu compra ha sido concretada con exito',
					  imageUrl: 'http://50.21.179.35:8083/bill-ok.png',
					  imageWidth: 150,
					  imageHeight: 150,
					  animation: false,
						confirmButtonText:"Continuar",
						showCancelButton: true,
					},function(success){
						if(success.value){
							$state.go('base.index');
						}
					});
				}
			})
		}
	}

	$scope.transactions.s._id = $stateParams.id;
	$scope.transactions.getSingle(function(success){
		if(success){
			let document = $scope.transactions.item.buyer.document.split('-')
			$scope.on.form.name = $scope.transactions.item.buyer.name+' '+$scope.transactions.item.buyer.lastname;
			$scope.on.form.id = document[1];
			$scope.on.form.idp = document[0];
			if($scope.transactions.item.buyer.localities.length>1)
				for(var i in $scope.transactions.item.buyer.localities)
					if($scope.on.form.address==undefined)
						$scope.on.form.address = $scope.transactions.item.buyer.localities[i].name;
					else
						$scope.on.form.address += ', '+$scope.transactions.item.buyer.localities[i].name;
		}
	})

	$rootScope.redirection = function(i) {
		switch(i){
			case undefined:
				$state.go('bankcheckout.bankcheckoutOne',{id: $stateParams.id,type: $scope.transactions.item.payments[0].type});
				break;
			case 1:
				$state.go('bankcheckout.bankcheckoutTwo',{id: $stateParams.id});
				break;
			case 2:
				$state.go('bankcheckout.bankcheckoutThird',{id: $stateParams.id});
				break;
		}
	}
}

function PublicationLikes(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,SweetAlert,Likes){
	$rootScope.title = $state.current.title;

	$scope.likes = angular.copy(Likes);

	$scope.items = [];
	$scope.total = 1000;
	$scope.limit = 2;
	$scope.flag = false;

	$scope.likes.s = {
		start: 0,
		limit: $scope.limit,
		byMe : true
	}

	if($stateParams.orderDate)
		$scope.likes.s.orderDate 	= $stateParams.orderDate ? $stateParams.orderDate: '';
		$scope.send.form.sort = $stateParams.orderDate ? $stateParams.orderDate: '';
	if($stateParams.orderSales)
		$scope.likes.s.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
		$scope.orderSales = $stateParams.orderSales ? $stateParams.orderSales: '';
	$scope.likes.s.name = $stateParams.search ? $stateParams.search: '';
	$scope.currentPage = $stateParams.page ? $stateParams.page: 1;
	$scope.likes.s.start = $scope.limit*($scope.currentPage-1);

	$scope.likes.get(function(success){
		if(success){
			$scope.items = $scope.likes.items;
			$scope.total = $scope.likes.total;
			if($scope.total>0)
				$scope.flag = true;
		}
	})

	$scope.setPage = function (pageNo) {
		$scope.currentPage = pageNo;
		console.log(pageNo)
	};

	$scope.pageChanged = function() {
		$state.go('.',{page: $scope.currentPage})
		console.log('Page changed to: ' + $scope.currentPage);
	};

	$scope.orderColum = {
    list: function(){},
    grid: function(){}
  }

	$scope.send = {
		form: {},
		search: function(){
			$state.go('.',{search: this.form.search,page: 1});
		},
		filter: function(options){
			if(options){
				this.form.filter = options;
				$state.go('.',{orderSales: this.form.filter});
			}else {
				$state.go('.',{orderDate: this.form.sort});
			}
		}
	}

	if($stateParams.search)
		$scope.send.form.search = $stateParams.search;
}

function PublicationEditController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Publications,Users,Category,Views,Comments,SweetAlert,Transactions,Upload,Locality){

	$scope.publications = angular.copy(Publications)

	$scope.photosOld = [];
	$scope.photos = [];
	$scope.inputPhotos = [];
	$scope.outPhotos = [];
	$scope.i = 0;
	$scope.flag = false;

	$scope.PhotoEdit = function(index){
		$scope.i = index;
		$('#Preview').modal('show');
		Upload.dataUrl($scope.photos[index],false)
		.then(function(urls){
			if(urls)
				$scope.inputPhotos[index] = urls;
				// $scope.disabled[index] = true;
		});
	}
	$scope.disabled = function(index){
		$scope.disabled[index] 	= false;
		$scope.photos[index] 		= false;
		$scope.outPhotos[index] = false;
	}

	$scope.on = {
		form: {},
		submit: function(){

			let data = {},
					publication = $scope.publications.item;
			if(this.form.name != publication.name)
				data.name = this.form.name
			if(this.form.body != publication.body)
				data.body = this.form.body;
			if(this.form.state != publication.state)
				data.state = this.form.state;
			if(this.form.stock != publication.stock)
				data.stock = this.form.stock;
			if(this.form.price != publication.price)
				data.price = this.form.price;
			if(this.form.waranty != publication.waranty)
				data.waranty = this.form.waranty;
			if(this.form.physical != publication.physical)
				data.physical = this.form.physical;
			if(this.form.invoice != publication.invoice)
				data.invoice = this.form.invoice;
			data._id = this.form._id;
			data.localities = $scope.onLocality.out;

			console.log(data)

			data.files64 = [];
			if($scope.outPhotos.length > 0){
				for(var x in $scope.outPhotos)
					if($scope.outPhotos[x]!=undefined)
						data.files64[x] = $scope.outPhotos[x];
			}
			if(data.files64.length<1){
				return SweetAlert.swal({
					title:'No has incluido imagenes de tu producto',
					text: 'Debes incluir al menos un imagen de tu producto para poder ser publicado',
					type: 'error'
				},function(success){})
			}

			if(data.name || data.body || data.state || data.stock || data.price || data.waranty || data.physical || data.invoice || data.files64){
				$scope.publications.p = data;
				$('#btnCancel').hide()
				$('#btnContinue').button('loading')
				$scope.publications.put(function(success){
					$('#btnContinue').button('reset')
					$('#btnCancel').show()
					if(success){
						return SweetAlert.swal({
							title: 'Modificacion Exitosa',
							text: 'Se actualizo correctamente.',
							imageUrl: 'http://50.21.179.35:8083/bill-ok.png',
						  imageWidth: 150,
						  imageHeight: 150,
						  animation: false,
							confirmButtonText:"Continuar",
							showCancelButton: true
						},function(success){
							if(success)
								$state.go('sales.publications',{state: 'enabled', page: 1, search: undefined})
						})
					}
				})
			}
		}
	}

	$scope.onLocality = {
		action: angular.copy(Locality),
		collections: [],
		out: [],
		parent: function(i){
			this.action.getChilds(this.out[i-1],function(success,items){
				if(success)
					this.collections[i] = items;
			}.bind(this))
		},
		init: function(){
			this.action.s.parent = false;
			this.action.get(function(success){
				if(success)
					this.collections[0] = this.action.items
			}.bind(this),false)
		}
	}
	$scope.onLocality.init();

	$scope.publications.getSingle($stateParams.id,function(success){
		if(success){
			console.log($scope.publications.item)
			$scope.on.form.name 		= $scope.publications.item.name;
			$scope.on.form.body 		= $scope.publications.item.body;
			$scope.on.form.state 		= $scope.publications.item.state;
			$scope.on.form.stock 		= $scope.publications.item.stock;
			$scope.on.form.price 		= $scope.publications.item.price;
			$scope.on.form.waranty	= $scope.publications.item.waranty;
			$scope.on.form.physical	= $scope.publications.item.physical;
			$scope.on.form.invoice	= $scope.publications.item.invoice;
			$scope.on.form._id			= $scope.publications.item._id;
		}
	},false)

}

function TermConditionController(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,SweetAlert,Valuepair){
	$rootScope.title = $state.current.title;

	$scope.valuepair = angular.copy(Valuepair);
	$scope.valuepair.get(function(success){
		console.log($scope.valuepair.items)
	})
}

function detailsTransactions(GeoPosition,toastr,$rootScope,$state,$scope,$http,$location,$stateParams,$cookies,$cookieStore,$document,$window,$timeout,Auth,Transactions,SweetAlert,Reports,ReportComments){
	$rootScope.title = $state.current.title;

	if(Auth.user){
		$scope.user = angular.copy(Auth.user);
	}else {
		$rootScope.$on('gotUser',function(){
			$scope.user = angular.copy(Auth.user);
		})
	}

	$scope.transactions = angular.copy(Transactions);
	$scope.reports      = angular.copy(Reports);
	$scope.reportcomment = angular.copy(ReportComments)

	$scope.banksaccount = [
			{
				name: 'Venezuela',
				value: 1
			},
			{
				name: 'Banesco',
				value: 2
			},
			{
				name: 'Provincial',
				value: 3
			},
			{
				name: 'Mercantil',
				value: 4
			},
			{
				name: 'BOD',
				value: 5
			},
			{
				name: 'Bicentenario',
				value: 6
			},
			{
				name: 'Del Tesoro',
				value: 7
			},
			{
				name: 'Bancaribe',
				value: 8
			},
			{
				name: 'Exterior',
				value: 9
			},
			{
				name: 'BNC',
				valule: 10
			},
			{
				name: 'BFC',
				value: 11
			},
			{
				name: 'BVC',
				value: 12
			},
			{
				name: 'Industrial',
				value: 13
			},
			{
				name: 'Caroní',
				value: 14
			},
			{
				name: 'Sofitasa',
				value: 15
			},
			{
				name: 'Banplus',
				value: 16
			},
			{
				name: 'Plaza',
				value: 17
			},
			{
				name: 'Activo',
				value: 18
			},
			{
				name: 'Bancrecer',
				value: 19
			},
			{
				name: 'Del Sur',
				value: 20
			},
			{
				name: '100% Banco',
				value: 21
			},
			{
				name: 'Agricola',
				value: 22
			},
			{
				name: 'Citibank',
				value: 23
			},
			{
				name: 'BANFANB',
				value: 24
			},
			{
				name: 'Bancamiga',
				value: 25
			},
			{
				name: 'Bangente',
				value: 26
			},
			{
				name: 'Mi Banco',
				value: 27
			},
			{
				name: 'Novo Banco',
				value: 28
			}
		]

	$scope.agency = [
		'Zoom',
		'MRW',
		'Entrega Personal',
		'DHL',
		'Domesa',
		'Ipostel',
		'Tealca'
	]

	function transaction(){
		$scope.transactions.s._id = $stateParams.id;
		$scope.transactions.getSingle(function(success){
			if(success){
				console.log($scope.transactions.item)
				$scope.transactions.item.cancelPrice = 0;
				$scope.transactions.item.remainingPrice = $scope.transactions.item.price;
				for (var items of $scope.transactions.item.payments){
					$scope.transactions.item.cancelPrice += parseInt(items.sum);
					if($scope.transactions.item.remainingPrice > 0)
					$scope.transactions.item.remainingPrice -= parseInt(items.sum);
				}
				$scope.trans = angular.copy($scope.transactions.item);
				$scope.reportcomment.s._id  = $scope.transactions.item._id;
				$scope.reportcomment.get((success) => {
					if(success)
						console.log($scope.reportcomment.item);
				})
			}
		});
	}

	$scope.on = {
		form: {},
		billing: function(){
			$('#btn-billing').button('loading')
			let data = {};
			if(!$scope.billing.name)
				data = this.form;
			else {
				if(this.form.name != $scope.billing.name)
					data.name = this.form.name;
				if(this.form.id != $scope.billing.id)
					data.id = this.form.id;
				if(this.form.idp != $scope.billing.idp)
					data.idp = this.form.idp;
				if(this.form.address != $scope.billing.address)
					data.address = this.form.address;
			}
			if(data.name || data.id || data.idp || data.address){
				$scope.transactions.p = data;
				$scope.transactions.p._id = $stateParams.id;
				$scope.transactions.put(1,function(success){
					$('#btn-billing').button('reset')
					if(success){
						$('#billing').modal('hide')
						transaction();
					}else
					SweetAlert.swal({
						title:'Ha Ocurrido un error',
						// text: 'Debes incluir al menos un imagen de tu producto para poder ser publicado',
						type: 'error'
					},function(success){})
				})
			}else {
				$('#btn-billing').button('reset')
				$('#billing').modal('hide')
			}
		},
		delivery: function(){
			$('#btn-delivery').button('loading');
			let data = {};
			if(!$scope.delivery.name)
				data = this.form;
			else {
				if(this.form.name != $scope.delivery.name)
					data.name = this.form.name;
				if(this.form.address != $scope.delivery.address)
					data.address = this.form.address;
				if(this.form.agency != $scope.delivery.agency)
					data.agency = this.form.agency;
				if(this.form.id != $scope.delivery.id)
					data.id = this.form.id;
				if(this.form.idp != $scope.delivery.idp)
					data.idp = this.form.idp;
				if(this.form.ref != $scope.delivery.ref)
					data.ref = this.form.ref;
				if(this.form.file != $scope.delivery.file)
					data.file = this.form.file;
			}
			if(data.name || data.address || data.agency || data.id || data.idp || data.ref || data.file){
				$scope.transactions.p = data;
				$scope.transactions.p._id = $stateParams.id;
				$scope.transactions.put(2,function(success){
					$('#btn-delivery').button('reset')
					if(success){
						$('#delivery').modal('hide')
						transaction();
					}else
					SweetAlert.swal({
						title:'Ha Ocurrido un error',
						// text: 'Debes incluir al menos un imagen de tu producto para poder ser publicado',
						type: 'error'
					},function(success){})
				})
			}else
				$('#btn-delivery').button('reset')
				$('#delivery').modal('hide')
		},
		rating: function(){
			this.form = $scope.rating;
			// $scope.transactions.put(0,function(success){
			// 	if(success){}
			// })
			console.log(this.form)
		}
	}

	$scope.modal = {
		billing: function(){
			$scope.on.form = {};
			console.log('billing')
			$('#billing').modal('show')
			$scope.billing = $scope.trans.billing;
			if($scope.trans.billing.id){
				$scope.on.form = $scope.trans.billing;
			}else {
				let document = $scope.user.document.split('-');
				$scope.on.form.name = `${$scope.user.name} ${$scope.user.lastname}`;
				$scope.on.form.idp	= document[0];
				$scope.on.form.id 	= document[1];
				$scope.on.form.address = [];
				for (var variable in $scope.user.localities) {
					$scope.on.form.address[variable] = $scope.user.localities[variable].name;
				}
				$scope.on.form.address = $scope.on.form.address.toString();
			}
		},
		rating: function(){
			$scope.on.form = {};
			console.log('rating')
			$('#rating').modal('show')
			$scope.rating = $scope.trans.rating;
			if($scope.trans.rating)
				$scope.on.form = $scope.trans.rating;
			// else
		},
		delivery: function(){
			$scope.on.form = {};
			console.log('delivery')
			$('#delivery').modal('show')
			$scope.delivery = $scope.trans.delivery;
			if($scope.trans.delivery.address){
				$scope.on.form = angular.copy($scope.trans.delivery);
			}else {
				let document = $scope.user.document.split('-');
				$scope.on.form.name = `${$scope.user.name} ${$scope.user.lastname}`;
				$scope.on.form.idp	= document[0];
				$scope.on.form.id 	= document[1];
				$scope.on.form.address = [];
				for (var variable in $scope.user.localities) {
					$scope.on.form.address[variable] = $scope.user.localities[variable].name;
				}
				$scope.on.form.address = $scope.on.form.address.toString();
			}
		}
	}
	transaction();
}
