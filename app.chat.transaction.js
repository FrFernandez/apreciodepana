var express = require('express'),
	compression = require('compression'),
	minify = require('express-minify'),
	cors = require('cors'),
	path = require('path'),
	logger = require('morgan'),
	ObjectID = require('mongoskin').ObjectID,
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	User = require('./routes/objects/user'),
	Transaction = require('./routes/objects/transaction'),
	TransMessage = require('./routes/objects/transMessage'),
	Notification = require('./routes/objects/notification'),
	Server = require('socket.io'),
	io = new Server(),
	app = express(),
	NodeGeocoder = require('node-geocoder'),
	geocoder = NodeGeocoder({
		provider: 'google',
		httpAdapter: 'https',
		apiKey: 'AIzaSyDxPb9rcUgWEK2OBqHtI67qPKSfQb998Hc'
	}),
	corsOptions = {
		origin: ["*:*", "https://qllevo.com", "https://www.qllevo.com", "https://admin.qllevo.com"],
		optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
	};
	
User = new User();
Transaction = new Transaction();
TransMessage = new TransMessage();
Notification = new Notification();

//TransMessage.remove({});

User.update({}, {"$set":{online:false}}, {multi: true});

app.use(compression());	
app.use(cors(corsOptions));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

io.origins('*:* https://qllevo.com:* https://www.qllevo.com:* https://admin.qllevo.com:*');

var clients = [];

//Transaction.update({_id: new ObjectID("590d1cb9fccd9cf61347b150")}, {"$set": {indexH: 0, indexU: 0}}, {multi: true});
//Transaction.update({_id: new ObjectID("590d4eb826232123f1746a0f")}, {"$set": {indexH: 1, indexU: 1}}, {multi: true});

function insertTransMessage(mID, mB, findSol, findMes, unread, transaction, user){
	mB.type = 1;
	return TransMessage.insert(mB, function(success, transMessage){
		if(success){
			var solNotFound = findSol ? true : false, 
				mesNotFound = findMes ? true : false;
			
			for(let i = 0; i < clients.length; i++){
				if(clients[i].userID.equals(mID)){
					for(let j = 0; j < clients[i].sockets.length; j++){
						if(clients[i].sockets[j].subbedTo.equals(mB.transaction)){
							console.log("byu");
							io.to(clients[i].sockets[j].id).emit("byYou", transMessage);
						}
					}
				}else if(findSol && clients[i].userID.equals(findSol)){
					for(let j = 0; j < clients[i].sockets.length; j++){
						transMessage.bep = true;
						if(clients[i].sockets[j].subbedTo.equals(mB.transaction)){
							console.log("sol found");
							solNotFound = false;
							io.to(clients[i].sockets[j].id).emit("toYou", transMessage);
							transMessage.bep = false;
						}
					}
				}else if(findMes && clients[i].userID.equals(findMes)){
					for(let j = 0; j < clients[i].sockets.length; j++){
						transMessage.bep = true;
						console.log(clients[i].sockets[j].subbedTo);
						if(clients[i].sockets[j].subbedTo.equals(mB.transaction)){
							console.log("mes found");
							mesNotFound = false;
							io.to(clients[i].sockets[j].id).emit("toYou", transMessage);
							transMessage.bep = false;
						}
					}
				}else if(clients[i].priv == 1){
					for(let j = 0; j < clients[i].sockets.length; j++){
						transMessage.bep = true;
						if(clients[i].sockets[j].subbedTo.equals(mB.transaction)){
							io.to(clients[i].sockets[j].id).emit("toYou", transMessage);
							transMessage.bep = false;
						}
					}
				}
			}
			
			Transaction.update({_id: mB.transaction}, unread);
			Transaction.update({_id: mB.transaction}, {"$set":{lastActive: Date.now()}});
			
			if(solNotFound){
				console.log("sol not found");
				Notification.insert({
					subject: "Has recibido un nuevo mensaje de "+user.name,
					transMessage: transMessage.transMessage,
					user: findSol,
					category: 2,
					meta: mB.transaction
				});
			}
			
			if(mesNotFound){
				console.log("mes not found");
				Notification.insert({
					subject: "Has recibido un nuevo mensaje de "+user.name,
					transMessage: transMessage.transMessage,
					user: findMes,
					category: 2,
					meta: mB.transaction
				});
			}
			
			
			return;
		}
	});
}
	
var setLastOnline = function(socketID){
	let found = false;
	for(let i = 0 ; i < clients.length; i++){
		for(let j = 0 ; j < clients[i].sockets.length; j++)
			if(clients[i].sockets[j].id == socketID){
				clients[i].sockets[j].lastOnline = Date.now();
				found = true;
				break;
			}
		if(found) break;	
			
	}
}

setInterval(function(){
	let remove = [];
	
	for(let i = 0; i < clients.length; i++){
		for(let j = 0; j < clients[i].sockets.length ; j++){
			if(clients[i].sockets[j].lastOnline < Date.now() - 10 * 60 * 1000){
				remove.push(clients[i].sockets[j].id);
			}
		}
	}
	
	for(let i = 0; i < remove.length; i++)
		if(io.sockets.connected[remove[i]])
			io.sockets.connected[remove[i]].disconnect(true);
}, 10 * 60 * 1000);
			
io.on('connection', function(socket, next){
	if(!ObjectID.isValid(socket.handshake.query.transaction))
		return socket.disconnect();
	
	return User.checkHash(socket.handshake.query.token, socket.handshake.query.tokensecret, function(user){
		if(user){
            let notFound = true;
				
				
			for(let i = 0; i < clients.length; i++)
				if(clients[i].userID.equals(new ObjectID(user._id))){
					clients[i].sockets.push({
						id: socket.id,
						lastOnline: Date.now(),
						subbedTo: new ObjectID(socket.handshake.query.transaction)
					});
					notFound = false;
				}
				
			if(notFound){
				clients.push({
					userID: new ObjectID(user._id),
					priv: user.priv,
					sockets: [
						{
							id: socket.id,
							lastOnline: Date.now(),
							subbedTo: new ObjectID(socket.handshake.query.transaction)
						}
					]
				});
				User.update({_id: new ObjectID(user._id)}, {"$set":{online: true}});
			}
			
			console.log(clients);
			
			socket.on('disconnect', function(){
				console.log("disconnected", user._id);
				for(var i = 0; i < clients.length; i++){
					console.log(clients[i].userID);
					if(clients[i].userID.equals(new ObjectID(user._id))){
						for(var j = 0; j < clients[i].sockets.length ; j++){
							if(clients[i].sockets[j].id == socket.id){
								clients[i].sockets.splice(j,1);
								if(clients[i].sockets.length === 0){
									User.update({_id: clients[i].userID}, {"$set":{online: false}});
									clients.splice(i,1);
								}
							}
							break;
						}
						break;
					}
				}
			});
			
			socket.on('read', function(data){
				setLastOnline(socket.id);
				data.to = new ObjectID(data.to);
				let find = {_id: data.to, "$or":[{seller: new ObjectID(user._id), buyer: new ObjectID(user._id)}]};
				if(new ObjectID(user._id).equals(new ObjectID(transaction.seller))){
					find.house = new ObjectID(user._id);
					find["$set"] = {unreadS: 0};
				}else{
					find.user = new ObjectID(user._id);
					find["$set"] = {unreadB: 0};
				}
				
				for(let i = 0; i < clients.length; i++)
					if(clients[i].userID.equals(new ObjectID(user._id)))
						for(let j = 0; j < clients[i].sockets.length; j++)
							io.to(clients[i].sockets[j].id).emit("read", {});
				
				return Transaction.update(find, read);
			});
			
			socket.on('mtransMessage', function(data){
				setLastOnline(socket.id);
				
				data.to = new ObjectID(data.to);
				
				let find = {
					_id: data.to
				};
				
				if(!User.isAdmin(user))
					find["$or"] = [
						{seller: new ObjectID(user._id)}, 
						{buyer: new ObjectID(user._id)}
					];
				
				return Transaction.findOne(find, function(success, transaction){
					if(success){
						
						var findSol, findMes, unread = {"$inc":{}};
						
						if(new ObjectID(user._id).equals(new ObjectID(transaction.buyer))){
							unread["$inc"] = {unreadS: 1};
							if(ObjectID.isValid(transaction.seller))
								findMes = new ObjectID(transaction.seller);
						}else if(transaction.seller && new ObjectID(user._id).equals(new ObjectID(transaction.seller))){
							unread["$inc"] = {unreadB: 1};
							findSol = new ObjectID(transaction.buyer);
						}else if(User.isAdmin(user)){
							findSol = new ObjectID(transaction.buyer);
							if(ObjectID.isValid(transaction.seller))
								findMes = new ObjectID(transaction.seller);
						}
						
						
						
						var mB = {
							transaction: data.to,
							transMessage: data.transMessage,
							user: user,
							type: 1,
							bep: true
						};
						return insertTransMessage(new ObjectID(user._id), mB, findSol, findMes, unread, transaction, user);
					}
				});
			});
			//return next();
		}else{
			//console.log(socket);
			socket.disconnect();
		}
	});
});

module.exports = {
	app: app,
	io: io
};